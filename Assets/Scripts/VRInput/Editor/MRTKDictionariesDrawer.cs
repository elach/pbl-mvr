﻿using Microsoft.MixedReality.Toolkit.Utilities;
using UnityEngine;
using VRInput.Runtime;

[UnityEditor.CustomPropertyDrawer(typeof(StringHandPoseDictionary))]
public class StringHandPoseDictionaryDrawer : SerializableDictionaryDrawer<string, HandPose> {

    protected override SerializableKeyValueTemplate<string, HandPose> GetTemplate() {
        return GetGenericTemplate<SerializableStringHandPoseTemplate>();
    }
}

internal class SerializableStringHandPoseTemplate : SerializableKeyValueTemplate<string, HandPose> { }

[UnityEditor.CustomPropertyDrawer(typeof(TrackedHandJointTransformDictionary))]
public class TrackedHandJointTransformDictionaryDrawer : SerializableDictionaryDrawer<TrackedHandJoint, Transform> {

    protected override SerializableKeyValueTemplate<TrackedHandJoint, Transform> GetTemplate() {
        return GetGenericTemplate<SerializableTrackedHandJointTransformTemplate>();
    }
}

internal class SerializableTrackedHandJointTransformTemplate : SerializableKeyValueTemplate<TrackedHandJoint, Transform> { }