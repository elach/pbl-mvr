﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.Utilities;
using Newtonsoft.Json;
using UnityEngine;

namespace VRInput.Runtime
{
    public class HandPose {
        public enum HandednessResult { NotCompatible, AsIs, Mirrored }


        public readonly Handedness setupHand;
        public readonly  Finger thumb = new Finger(FingerType.Thumb);
        public readonly Finger index = new Finger(FingerType.Index);
        public readonly Finger middle = new Finger(FingerType.Middle);
        public readonly Finger ring = new Finger(FingerType.Ring);
        public readonly Finger pinky = new Finger(FingerType.Pinky);
        public readonly MixedRealityPose wristPose;
        public readonly  MixedRealityPose palmPose;

        public Dictionary<TrackedHandJoint, MixedRealityPose> allPoses { private set; get; }
        public Dictionary<FingerJointAddress, MixedRealityPose> allFingerPoses { private set; get; }

        public MixedRealityPose GetMRPose(FingerJointAddress addr) {
            if (addr.isFinger) {
                return allFingerPoses[addr];
            }
            return new MixedRealityPose();
        }

        public MixedRealityPose GetMRPose(TrackedHandJoint joint) {
            return allPoses[joint];
        }
        
        public MixedRealityPose GetMRPoseFlipped(TrackedHandJoint joint) {
            Vector3 handUp = wristPose.Up;
            MixedRealityPose baseRot = allPoses[joint];
            return new MixedRealityPose(baseRot.Position, FlipQ(baseRot.Rotation, handUp));
        }
        
        Quaternion FlipQ(Quaternion q, Vector3 reflectionVector) {
            Vector3 lRot = q * Vector3.up;
            Vector3 diff = reflectionVector * Vector3.Dot(lRot, reflectionVector);
            return Quaternion.LookRotation(lRot - 2 * diff, Vector3.up);
        }

        public IEnumerable<Finger> GetFingers() {
            yield return thumb;
            yield return index;
            yield return middle;
            yield return ring;
            yield return pinky;
        }
        
        public HandednessResult HandednessCheck<T>(InputEventData<T> eventData, bool allowBothHands) {
            Handedness currentHand = eventData.Handedness;
            bool correctHand = currentHand == setupHand;
            if (correctHand) {
                return HandednessResult.AsIs;
            }
            else if (allowBothHands) {
                return HandednessResult.Mirrored;
            }
            else {
                return HandednessResult.NotCompatible;
            }
        }        

        public Finger GetFinger(FingerType type) {
            switch (type) {
                case FingerType.Thumb:
                    return thumb;

                case FingerType.Index:
                    return index;

                case FingerType.Middle:
                    return middle;

                case FingerType.Ring:
                    return ring;

                case FingerType.Pinky:
                    return pinky;

            }
            return null;
        }

        public HandPose(Handedness setupHand, MixedRealityPose wristPose, MixedRealityPose palmPose, params Finger[] fingers) {
            this.setupHand = setupHand;
            this.thumb = fingers[0];
            this.index =  fingers[1];
            this.middle =  fingers[2];
            this.ring =  fingers[3];
            this.pinky =  fingers[4];
            this.wristPose = wristPose;
            this.palmPose = palmPose;
            CreateCachedValues();
        }
        
        public HandPose(IMixedRealityHand hand) {
            if (hand != null) {
                hand.TryGetJoint(TrackedHandJoint.Wrist, out wristPose);
                hand.TryGetJoint(TrackedHandJoint.Palm, out palmPose);
                thumb = new Finger(FingerType.Thumb, hand);
                index = new Finger(FingerType.Index, hand);
                middle = new Finger(FingerType.Middle, hand);
                ring = new Finger(FingerType.Ring, hand);
                pinky = new Finger(FingerType.Pinky, hand);
                setupHand = hand.ControllerHandedness;
            }
            CreateCachedValues();
        }

        void CreateCachedValues() {
            allFingerPoses = GetFingers()
                .Where(finger => finger != null)
                .SelectMany(finger => finger.GetStages())
                .ToDictionary(pair => pair.Key, pair => pair.Value);
            allPoses = allFingerPoses.ToDictionary(
                pair => pair.Key.joint,
                pair => pair.Value
            );
            allPoses.Add(TrackedHandJoint.Wrist, wristPose);
            allPoses.Add(TrackedHandJoint.Palm, palmPose);
        }


    }
}