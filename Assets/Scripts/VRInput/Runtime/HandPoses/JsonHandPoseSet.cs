﻿using System;
using System.Diagnostics;
using System.Globalization;
using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.Utilities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.UnityConverters.Math;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace VRInput.Runtime {
    [Serializable]
    class JsonHandPoseSet {
        [SerializeField] Handedness handedness;
        [SerializeField] string thumbJson;
        [SerializeField] string indexJson;
        [SerializeField] string middleJson;
        [SerializeField] string ringJson;
        [SerializeField] string pinkyJson;
        [SerializeField] string wristJson;
        [SerializeField] string palmJson;

        public HandPose CreatePose() {
            return new HandPose(
                handedness,
                FindPose(wristJson),
                FindPose(palmJson),
                JsonConvert.DeserializeObject<Finger>(thumbJson),
                JsonConvert.DeserializeObject<Finger>(indexJson),
                JsonConvert.DeserializeObject<Finger>(middleJson),
                JsonConvert.DeserializeObject<Finger>(ringJson),
                JsonConvert.DeserializeObject<Finger>(pinkyJson));
        }

        public JsonHandPoseSet(HandPose fromPose) {
            handedness = fromPose.setupHand;
            thumbJson = JsonConvert.SerializeObject(fromPose.thumb);
            indexJson = JsonConvert.SerializeObject(fromPose.index);
            middleJson = JsonConvert.SerializeObject(fromPose.middle);
            ringJson = JsonConvert.SerializeObject(fromPose.ring);
            pinkyJson = JsonConvert.SerializeObject(fromPose.pinky);
            wristJson = JsonConvert.SerializeObject(fromPose.wristPose);
            palmJson = JsonConvert.SerializeObject(fromPose.palmPose);
        }

        const string posName = "Position";
        const string rotName = "Rotation";
        
        MixedRealityPose FindPose(string json) {
            MixedRealityPose pose = default;
            JObject jObj  = JObject.Parse(json);
            try {
                pose.Position = new Vector3(
                    float.Parse((string) jObj[posName]["x"], CultureInfo.InvariantCulture),
                    float.Parse((string) jObj[posName]["y"], CultureInfo.InvariantCulture),
                    float.Parse((string) jObj[posName]["z"], CultureInfo.InvariantCulture));

                pose.Rotation = new Quaternion(
                    float.Parse((string) jObj[rotName]["x"], CultureInfo.InvariantCulture),
                    float.Parse((string) jObj[rotName]["y"], CultureInfo.InvariantCulture),
                    float.Parse((string) jObj[rotName]["z"], CultureInfo.InvariantCulture),
                    float.Parse((string) jObj[rotName]["w"], CultureInfo.InvariantCulture));
            }
            catch(NullReferenceException nullRef) {
                Debug.LogError("Missing values in json.");
            }
            catch(FormatException nullRef) {
                Debug.LogError("Invalid values in json." + json );
            }
            return pose;
        }
        
    }
}
