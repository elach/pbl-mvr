﻿using System;
using UniRx;

namespace VRInput.Runtime {
    [Serializable]
    public class HandPoseDetectionDataReactiveProperty : ReactiveProperty<HandPoseDetectionData> {
        public HandPoseDetectionDataReactiveProperty(HandPoseDetectionData data) : base(data) { }
    }
}