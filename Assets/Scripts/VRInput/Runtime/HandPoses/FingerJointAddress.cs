﻿using System;
using Microsoft.MixedReality.Toolkit.Utilities;
using UnityEngine;

namespace VRInput.Runtime
{
    [System.Flags]
    [Serializable]
    public enum FingerType {
        Thumb   =   0b001_00000,
        Index   =   0b010_00000,
        Middle  =   0b011_00000,
        Ring    =   0b100_00000,
        Pinky   =   0b101_00000
    }
    
    [Serializable]
    [System.Flags]
    public enum JointStage {
        Metacarpal= 0b000_00001,
        Knuckle   = 0b000_00010,
        Middle    = 0b000_00100,
        Proximal  = 0b000_00110, //thumb only
        Distal    = 0b000_01000,
        Tip       = 0b000_10000
    }

    public struct FingerJointAddress {
        public readonly bool isFinger;

        public FingerType finger { private set; get; }

        public JointStage stage { private set; get; }

        public TrackedHandJoint joint { private set; get; }

        public FingerJointAddress(Int32 binaryRepresentation) {
            if (binaryRepresentation == 0) {
                isFinger = false;
            }
            else {
                isFinger = true;
            }
            finger = (FingerType)(binaryRepresentation & 0b111_00000);
            stage = (JointStage)(binaryRepresentation & 0b000_11111);
            joint = ToJoint(finger, stage);
        }

        public FingerJointAddress(TrackedHandJoint joint) {
            this.joint = joint;
            Int32 binaryRepresentation = FromMRTK(joint);
            if (binaryRepresentation == 0) {
                isFinger = false;
            }
            else {
                isFinger = true;
            }
            finger = (FingerType)(binaryRepresentation & 0b111_00000);
            stage = (JointStage)(binaryRepresentation & 0b000_11111);

        }

        public FingerJointAddress(FingerType type, JointStage stage) {
            finger = type;
            this.stage = stage;
            isFinger = true;
            joint = ToJoint(type, stage);

        }

        public Int32 ToInt() {
            return (Int32)finger | (Int32)stage;
        }

        #region TRAVERSAL

        public FingerJointAddress? PrevStage() {
            if (finger == FingerType.Thumb) {
                if (stage == JointStage.Distal) {
                    return new FingerJointAddress(finger, JointStage.Proximal);
                }
            }
            switch (stage) {
                case JointStage.Metacarpal:
                    return null;

                case JointStage.Knuckle:
                    return new FingerJointAddress(finger, JointStage.Metacarpal);

                case JointStage.Middle:
                    return new FingerJointAddress(finger, JointStage.Knuckle);

                case JointStage.Proximal:
                    return new FingerJointAddress(finger, JointStage.Metacarpal);

                case JointStage.Distal:
                    return new FingerJointAddress(finger, JointStage.Middle);

                case JointStage.Tip:
                    return new FingerJointAddress(finger, JointStage.Distal);
            }
            return null;
        }

        public FingerJointAddress? NextStage() {
            if (finger == FingerType.Thumb) {
                if (stage == JointStage.Metacarpal) {
                    return new FingerJointAddress(finger, JointStage.Proximal);
                }
            }
            switch (stage) {
                case JointStage.Metacarpal:
                    return new FingerJointAddress(finger, JointStage.Knuckle);

                case JointStage.Knuckle:
                    return new FingerJointAddress(finger, JointStage.Middle);

                case JointStage.Middle:
                    return new FingerJointAddress(finger, JointStage.Distal);

                case JointStage.Proximal:
                    return new FingerJointAddress(finger, JointStage.Distal);

                case JointStage.Distal:
                    return new FingerJointAddress(finger, JointStage.Tip);

                case JointStage.Tip:
                    return null;
            }
            return null;
        }

        #endregion TRAVERSAL

        #region CONVERSIONS

        static TrackedHandJoint ToJoint(FingerType finger, JointStage stage) {
            switch (finger) {
                case FingerType.Thumb:
                    switch (stage) {
                        case JointStage.Metacarpal:
                            return TrackedHandJoint.ThumbMetacarpalJoint;

                        case JointStage.Proximal:
                            return TrackedHandJoint.ThumbProximalJoint;

                        case JointStage.Distal:
                            return TrackedHandJoint.ThumbDistalJoint;

                        case JointStage.Tip:
                            return TrackedHandJoint.ThumbTip;
                    }
                    break;

                case FingerType.Index:
                    switch (stage) {
                        case JointStage.Metacarpal:
                            return TrackedHandJoint.IndexMetacarpal;

                        case JointStage.Knuckle:
                            return TrackedHandJoint.IndexKnuckle;

                        case JointStage.Middle:
                            return TrackedHandJoint.IndexMiddleJoint;

                        case JointStage.Distal:
                            return TrackedHandJoint.IndexDistalJoint;

                        case JointStage.Tip:
                            return TrackedHandJoint.IndexTip;
                    }
                    break;

                case FingerType.Middle:
                    switch (stage) {
                        case JointStage.Metacarpal:
                            return TrackedHandJoint.MiddleMetacarpal;

                        case JointStage.Knuckle:
                            return TrackedHandJoint.MiddleKnuckle;

                        case JointStage.Middle:
                            return TrackedHandJoint.MiddleMiddleJoint;

                        case JointStage.Distal:
                            return TrackedHandJoint.MiddleDistalJoint;

                        case JointStage.Tip:
                            return TrackedHandJoint.MiddleTip;
                    }
                    break;

                case FingerType.Ring:
                    switch (stage) {
                        case JointStage.Metacarpal:
                            return TrackedHandJoint.RingMetacarpal;

                        case JointStage.Knuckle:
                            return TrackedHandJoint.RingKnuckle;

                        case JointStage.Middle:
                            return TrackedHandJoint.RingMiddleJoint;

                        case JointStage.Distal:
                            return TrackedHandJoint.RingDistalJoint;

                        case JointStage.Tip:
                            return TrackedHandJoint.RingTip;
                    }
                    break;

                case FingerType.Pinky:
                    switch (stage) {
                        case JointStage.Metacarpal:
                            return TrackedHandJoint.PinkyMetacarpal;

                        case JointStage.Knuckle:
                            return TrackedHandJoint.PinkyKnuckle;

                        case JointStage.Middle:
                            return TrackedHandJoint.PinkyMiddleJoint;

                        case JointStage.Distal:
                            return TrackedHandJoint.PinkyDistalJoint;

                        case JointStage.Tip:
                            return TrackedHandJoint.PinkyTip;
                    }
                    break;
            }
            return TrackedHandJoint.None;
        }

        static Int32 FromMRTK(TrackedHandJoint joint) {
            switch (joint) {
                case TrackedHandJoint.ThumbMetacarpalJoint: return 0b001_00001;
                case TrackedHandJoint.ThumbProximalJoint: return 0b001_00110;
                case TrackedHandJoint.ThumbDistalJoint: return 0b001_01000;
                case TrackedHandJoint.ThumbTip: return 0b001_10000;
                case TrackedHandJoint.IndexMetacarpal: return 0b010_00001;
                case TrackedHandJoint.IndexKnuckle: return 0b010_00010;
                case TrackedHandJoint.IndexMiddleJoint: return 0b010_00100;
                case TrackedHandJoint.IndexDistalJoint: return 0b010_01000;
                case TrackedHandJoint.IndexTip: return 0b010_10000;
                case TrackedHandJoint.MiddleMetacarpal: return 0b011_00001;
                case TrackedHandJoint.MiddleKnuckle: return 0b011_00010;
                case TrackedHandJoint.MiddleMiddleJoint: return 0b011_00100;
                case TrackedHandJoint.MiddleDistalJoint: return 0b011_01000;
                case TrackedHandJoint.MiddleTip: return 0b011_10000;
                case TrackedHandJoint.RingMetacarpal: return 0b100_00001;
                case TrackedHandJoint.RingKnuckle: return 0b100_00010;
                case TrackedHandJoint.RingMiddleJoint: return 0b100_00100;
                case TrackedHandJoint.RingDistalJoint: return 0b100_01000;
                case TrackedHandJoint.RingTip: return 0b100_10000;
                case TrackedHandJoint.PinkyMetacarpal: return 0b101_00001;
                case TrackedHandJoint.PinkyKnuckle: return 0b101_00010;
                case TrackedHandJoint.PinkyMiddleJoint: return 0b101_00100;
                case TrackedHandJoint.PinkyDistalJoint: return 0b101_01000;
                case TrackedHandJoint.PinkyTip: return 0b101_10000;
                default: return 0;
            }
        }

        #endregion CONVERSIONS

    }
}