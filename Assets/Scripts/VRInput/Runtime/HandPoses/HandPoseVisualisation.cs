﻿using System;
using Newtonsoft.Json;
using UnityEngine;

namespace VRInput.Runtime
{
    [RequireComponent(typeof(HandPoseRecognizer))]
    public class HandPoseVisualisation : MonoBehaviour {
        [NaughtyAttributes.Required]
        [SerializeField] BoneMapper previewPrefab = null;
        BoneMapper preview;
        HandPose pose => hpr.pose;

        HandPoseRecognizer hpr {
            get {
                if (_hpr == null) {
                    _hpr = GetComponent<HandPoseRecognizer>();
                }
                return _hpr;
            }
        }
        HandPoseRecognizer _hpr;

        SkinnedMeshRenderer gizmosMeshRenderer;
        
        void Start() {
            SetVisEnabled(true);
        }
        
        void OnEnable() {
            SetVisEnabled(true);
        }

        void OnDisable() {
            SetVisEnabled(false);
        }

        void OnDestroy() {
            if (preview == null) {
                return;
            }
            if (Application.isPlaying) {
                Destroy(preview.gameObject);
            }
            else {
                DestroyImmediate(preview.gameObject);
            }

        }

        void SetVisEnabled(bool e) {
            TrySpawn();
            if (e) {
                preview.SetPose(pose);
            }
            if (preview != null) {
                preview.gameObject.SetActive(e);
            }
        }

        void TrySpawn() {
            if (previewPrefab == null || pose == null) {
                return;
            }

            if (preview == null) {
                preview =  SpawnNewPreview();
            }

        }

        BoneMapper SpawnNewPreview() {
            BoneMapper p = Instantiate(previewPrefab, transform)
                .GetComponent<BoneMapper>();
            this.GetNotNullComponent<PoseRecognitionMaterialProvider>(provider => {
                Renderer r = p.GetComponentInChildren<Renderer>();
                r.material = provider.matInstance;
            });
            return p;
        }


        [NaughtyAttributes.Button]
        void ForceRefresh() {
            if (preview != null) {
                DestroyImmediate(preview.gameObject);
            }
            TrySpawn();
            preview.SetPose(pose);
            preview.SetPose(pose);
        }

        void OnDrawGizmosSelected() {
            if (previewPrefab == null ) {
                return;
            }
            if (gizmosMeshRenderer == null) {
                gizmosMeshRenderer = previewPrefab.GetComponentInChildren<SkinnedMeshRenderer>();
            }

            Transform t = transform;
            Gizmos.matrix = Matrix4x4.TRS(t.position, Quaternion.identity, t.localScale);
            Gizmos.DrawMesh(gizmosMeshRenderer.sharedMesh);
        }
    }
}