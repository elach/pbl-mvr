﻿using System.Collections.Generic;
using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.Utilities;
using UnityEngine;

namespace VRInput.Runtime {
    public interface IHandPoseComparer {
        /// <summary>
        /// </summary>
        /// <param name="pose">Poza referencyjna</param>
        /// <param name="eventData"></param>
        /// <param name="input">Wprowadzane dane obecnej ręki</param>
        /// <returns>Podobieństwo wykrycia pozy zazwyczaj będące od 0 do 1</returns>
        float PoseSimilarityFactor(IHandPoseProvider pose,
            InputEventData<IDictionary<TrackedHandJoint, MixedRealityPose>> eventData);

    }
}