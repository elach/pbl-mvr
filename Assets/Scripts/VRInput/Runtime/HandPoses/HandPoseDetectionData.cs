﻿using Microsoft.MixedReality.Toolkit.Utilities;
using UnityEngine;

namespace VRInput.Runtime {
    [System.Serializable]
    public struct HandPoseDetectionData {

        public bool detected => mDetected;
        [SerializeField] bool mDetected;
        public Handedness hand => mHand;
        [SerializeField] Handedness mHand;
        
        public HandPoseDetectionData(bool mDetected, Handedness mHand) {
            this.mDetected = mDetected;
            this.mHand = mHand;
        }
    }
}