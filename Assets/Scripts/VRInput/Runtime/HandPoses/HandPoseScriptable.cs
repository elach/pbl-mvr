﻿using Microsoft.MixedReality.Toolkit.Input;
using NaughtyAttributes;
using Newtonsoft.Json;
using UnityEngine;

namespace VRInput.Runtime
{
    [CreateAssetMenu(menuName = "VRInput/HandPoseSO")]
    public class HandPoseScriptable : ScriptableObject, IHandPoseProvider {

        [SerializeField] bool detectBothHands = false;
        [SerializeField] JsonHandPoseSet jsonSet;
        HandPose pose;

        public bool AllowBothHands() => detectBothHands;

        public HandPose GetPose() {
            if (pose == null) {
                pose = jsonSet.CreatePose();
            }
            return pose;
        }

        public void SavePose(HandPose newPose) {
            this.pose = newPose;
            jsonSet = new JsonHandPoseSet(newPose);
            #if UNITY_EDITOR
            UnityEditor.EditorUtility.SetDirty(this);
            #endif    
            Debug.Log("saved");
        }
    }
}