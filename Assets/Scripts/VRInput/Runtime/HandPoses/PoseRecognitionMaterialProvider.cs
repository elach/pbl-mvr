﻿using System;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UniRx;
using UnityEngine;
using UnityEngine.XR;
using VRInput.Runtime;

[RequireComponent(typeof(HandPoseRecognizer))]
public class PoseRecognitionMaterialProvider : MonoBehaviour {
    [NaughtyAttributes.Required]
    [SerializeField] Material baseMaterial;
    [SerializeField] Color baseColor = Color.cyan;
    [SerializeField] float colorChangeTreshold = 0.5f; 
    [SerializeField] Color badColor = Color.red; 
    [SerializeField] Color goodColor = Color.green; 
    [SerializeField] Color detectedColor = Color.blue; 
    
    public Material matInstance {
        get {
            if (_matInstance == null) {
                _matInstance = Instantiate(baseMaterial);
                ChangeRecognitionCloseness(0.0f);
            }
            return _matInstance;
        }
    }
    [ShowNonSerializedField]
    Material _matInstance;

    List<IDisposable> disposables = new List<IDisposable>();

    void OnEnable() {
        HandPoseRecognizer hpr = GetComponent<HandPoseRecognizer>();
        Material instanceCheck = matInstance;
        disposables.Add(hpr.poseComparer.recognized
            .Subscribe(SetRecognizedMat));
        disposables.Add(hpr.poseComparer.recognitionRemaining
            .Subscribe(ChangeRecognitionCloseness));
    }

    void OnDisable() {
        foreach (IDisposable disp in disposables) {
            disp.Dispose();
        }
        disposables.Clear();
    }

    void ChangeRecognitionCloseness(float remaining) {
        if (remaining < colorChangeTreshold) {
            _matInstance.color = Color.Lerp(goodColor, badColor, remaining);
        }
        else {
            _matInstance.color = baseColor;
        }
    }

    void SetRecognizedMat(HandPoseDetectionData detectionData) {
        if (detectionData.detected) {
            _matInstance.color = detectedColor;
        }
    }
}
