﻿using System.Collections.Generic;
using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.Utilities;
using UnityEngine;

namespace VRInput.Runtime
{
    public class BoneMapper : MonoBehaviour {

        [SerializeField] Handedness hand;

        HandPose poseRef;

        public GameObject GameObjectProxy => gameObject;
        public IMixedRealityController Controller { get; set; }

        public Transform Wrist;

        public Transform Palm;

        public Transform ThumbRoot;

        public bool ThumbRootIsMetacarpal = true;

        public Transform IndexRoot;

        public bool IndexRootIsMetacarpal = true;

        public Transform MiddleRoot;
        public bool MiddleRootIsMetacarpal = true;

        public Transform RingRoot;

        public bool RingRootIsMetacarpal = true;

        public Transform PinkyRoot;
        public bool PinkyRootIsMetacarpal = true;

        public bool ModelPalmAtLeapWrist = true;

        public bool DeformPosition = true;
    
        public bool ScaleLastFingerBone = true;

        public Vector3 ModelFingerPointing = new Vector3(0, 0, 0);

        public Vector3 ModelPalmFacing = new Vector3(0, 0, 0);

        private const float thumbFingerTipLength = 0.02167f;

        private const float indexingerTipLength = 0.01582f;
        private const float middleFingerTipLength = 0.0174f;
        private const float ringFingerTipLength = 0.0173f;
        private const float pinkyFingerTipLength = 0.01596f;

        private Dictionary<TrackedHandJoint, float> fingerTipLengths = new Dictionary<TrackedHandJoint, float>()
        {
            {TrackedHandJoint.ThumbTip, thumbFingerTipLength },
            {TrackedHandJoint.IndexTip, indexingerTipLength },
            {TrackedHandJoint.MiddleTip, middleFingerTipLength },
            {TrackedHandJoint.RingTip, ringFingerTipLength },
            {TrackedHandJoint.PinkyTip, pinkyFingerTipLength }
        };

        private Quaternion userBoneRotation {
            get {
                if (ModelFingerPointing == Vector3.zero || ModelPalmFacing == Vector3.zero) {
                    return Quaternion.identity;
                }
                return Quaternion.Inverse(Quaternion.LookRotation(ModelFingerPointing, -ModelPalmFacing));
            }
        }

        private Quaternion Reorientation() {
            return Quaternion.Inverse(Quaternion.LookRotation(ModelFingerPointing, -ModelPalmFacing));
        }

        private Dictionary<TrackedHandJoint, Transform> joints = new Dictionary<TrackedHandJoint, Transform>();

        public void SetPose(HandPose pose) {
            Init();
            //transform.rotation = Quaternion.identity;
            SetPose(pose.allPoses);
            Palm.transform.localPosition = Vector3.zero;
            //transform.rotation = transform.parent.rotation;
        }

        void SetPose(IDictionary<TrackedHandJoint, MixedRealityPose> values) {
            Transform jointTransform;
            // Apply updated TrackedHandJoint pose data to the assigned transforms
            foreach (TrackedHandJoint handJoint in values.Keys) {
                if (joints.TryGetValue(handJoint, out jointTransform)) {
                    if (jointTransform != null) {
                        if (handJoint == TrackedHandJoint.Palm) {
                            if (ModelPalmAtLeapWrist) {
                                Palm.position = values[TrackedHandJoint.Wrist].Position;
                            }
                            else {
                                Palm.position = values[TrackedHandJoint.Palm].Position;
                            }
                            Palm.rotation = values[TrackedHandJoint.Palm].Rotation * userBoneRotation;
                        }
                        else if (handJoint == TrackedHandJoint.Wrist) {
                            if (!ModelPalmAtLeapWrist) {
                                Wrist.position = values[TrackedHandJoint.Wrist].Position;
                                Wrist.position = values[TrackedHandJoint.Wrist].Position;
                            }
                        }
                        else {
                            // Finger joints
                            jointTransform.rotation = values[handJoint].Rotation * Reorientation();

                            if (DeformPosition) {
                                jointTransform.position = values[handJoint].Position;

                                if (ScaleLastFingerBone &&
                                    (handJoint == TrackedHandJoint.ThumbDistalJoint ||
                                     handJoint == TrackedHandJoint.IndexDistalJoint ||
                                     handJoint == TrackedHandJoint.MiddleDistalJoint ||
                                     handJoint == TrackedHandJoint.RingDistalJoint ||
                                     handJoint == TrackedHandJoint.PinkyDistalJoint)) {
                                    ScaleFingerTip(values, jointTransform, handJoint + 1, jointTransform.position);
                                }
                            }
                        }
                    }
                }
            }

            
        }

        void Init() {
            // Initialize joint dictionary with their corresponding joint transforms
            joints[TrackedHandJoint.Wrist] = Wrist;
            joints[TrackedHandJoint.Palm] = Palm;

            // Thumb joints, first node is user assigned, note that there are only 4 joints in the thumb
            if (ThumbRoot) {
                if (ThumbRootIsMetacarpal) {
                    joints[TrackedHandJoint.ThumbMetacarpalJoint] = ThumbRoot;
                    joints[TrackedHandJoint.ThumbProximalJoint] = RetrieveChild(TrackedHandJoint.ThumbMetacarpalJoint);
                }
                else {
                    joints[TrackedHandJoint.ThumbProximalJoint] = ThumbRoot;
                }
                joints[TrackedHandJoint.ThumbDistalJoint] = RetrieveChild(TrackedHandJoint.ThumbProximalJoint);
                joints[TrackedHandJoint.ThumbTip] = RetrieveChild(TrackedHandJoint.ThumbDistalJoint);
            }
            // Look up index finger joints below the index finger root joint
            if (IndexRoot) {
                if (IndexRootIsMetacarpal) {
                    joints[TrackedHandJoint.IndexMetacarpal] = IndexRoot;
                    joints[TrackedHandJoint.IndexKnuckle] = RetrieveChild(TrackedHandJoint.IndexMetacarpal);
                }
                else {
                    joints[TrackedHandJoint.IndexKnuckle] = IndexRoot;
                }
                joints[TrackedHandJoint.IndexMiddleJoint] = RetrieveChild(TrackedHandJoint.IndexKnuckle);
                joints[TrackedHandJoint.IndexDistalJoint] = RetrieveChild(TrackedHandJoint.IndexMiddleJoint);
                joints[TrackedHandJoint.IndexTip] = RetrieveChild(TrackedHandJoint.IndexDistalJoint);
            }

            // Look up middle finger joints below the middle finger root joint
            if (MiddleRoot) {
                if (MiddleRootIsMetacarpal) {
                    joints[TrackedHandJoint.MiddleMetacarpal] = MiddleRoot;
                    joints[TrackedHandJoint.MiddleKnuckle] = RetrieveChild(TrackedHandJoint.MiddleMetacarpal);
                }
                else {
                    joints[TrackedHandJoint.MiddleKnuckle] = MiddleRoot;
                }
                joints[TrackedHandJoint.MiddleMiddleJoint] = RetrieveChild(TrackedHandJoint.MiddleKnuckle);
                joints[TrackedHandJoint.MiddleDistalJoint] = RetrieveChild(TrackedHandJoint.MiddleMiddleJoint);
                joints[TrackedHandJoint.MiddleTip] = RetrieveChild(TrackedHandJoint.MiddleDistalJoint);
            }

            // Look up ring finger joints below the ring finger root joint
            if (RingRoot) {
                if (RingRootIsMetacarpal) {
                    joints[TrackedHandJoint.RingMetacarpal] = RingRoot;
                    joints[TrackedHandJoint.RingKnuckle] = RetrieveChild(TrackedHandJoint.RingMetacarpal);
                }
                else {
                    joints[TrackedHandJoint.RingKnuckle] = RingRoot;
                }
                joints[TrackedHandJoint.RingMiddleJoint] = RetrieveChild(TrackedHandJoint.RingKnuckle);
                joints[TrackedHandJoint.RingDistalJoint] = RetrieveChild(TrackedHandJoint.RingMiddleJoint);
                joints[TrackedHandJoint.RingTip] = RetrieveChild(TrackedHandJoint.RingDistalJoint);
            }

            // Look up pinky joints below the pinky root joint
            if (PinkyRoot) {
                if (PinkyRootIsMetacarpal) {
                    joints[TrackedHandJoint.PinkyMetacarpal] = PinkyRoot;
                    joints[TrackedHandJoint.PinkyKnuckle] = RetrieveChild(TrackedHandJoint.PinkyMetacarpal);
                }
                else {
                    joints[TrackedHandJoint.PinkyKnuckle] = PinkyRoot;
                }
                joints[TrackedHandJoint.PinkyMiddleJoint] = RetrieveChild(TrackedHandJoint.PinkyKnuckle);
                joints[TrackedHandJoint.PinkyDistalJoint] = RetrieveChild(TrackedHandJoint.PinkyMiddleJoint);
                joints[TrackedHandJoint.PinkyTip] = RetrieveChild(TrackedHandJoint.PinkyDistalJoint);
            }
        }

        private Transform RetrieveChild(TrackedHandJoint parentJoint) {
            if (joints[parentJoint] != null && joints[parentJoint].childCount > 0) {
                return joints[parentJoint].GetChild(0);
            }
            return null;
        }

        private float Distance(TrackedHandJoint joint1, TrackedHandJoint joint2) {
            if (joints[joint1] != null && joints[joint2] != null) {
                return Vector3.Distance(joints[joint1].position, joints[joint2].position);
            }
            return 0;
        }

        private void ScaleFingerTip(IDictionary<TrackedHandJoint, MixedRealityPose> values, Transform jointTransform, TrackedHandJoint fingerTipJoint, Vector3 boneRootPos) {
            // Set fingertip base bone scale to match the bone length to the fingertip.
            // This will only scale correctly if the model was constructed to match
            // the standard "test" edit-time hand model from the LeapMotion TestHandFactory.
            var boneTipPos = values[fingerTipJoint].Position;
            var boneVec = boneTipPos - boneRootPos;

            if (transform.lossyScale.x != 0f && transform.lossyScale.x != 1f) {
                boneVec /= transform.lossyScale.x;
            }
            var newScale = jointTransform.transform.localScale;
            var lengthComponentIdx = GetLargestComponentIndex(ModelFingerPointing);
            newScale[lengthComponentIdx] = boneVec.magnitude / fingerTipLengths[fingerTipJoint];
            jointTransform.transform.localScale = newScale;
        }

        private int GetLargestComponentIndex(Vector3 pointingVector) {
            var largestValue = 0f;
            var largestIdx = 0;
            for (int i = 0; i < 3; i++) {
                var testValue = pointingVector[i];
                if (Mathf.Abs(testValue) > largestValue) {
                    largestIdx = i;
                    largestValue = Mathf.Abs(testValue);
                }
            }
            return largestIdx;
        }

    }
}