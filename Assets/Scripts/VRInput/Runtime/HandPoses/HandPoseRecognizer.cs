﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.Utilities;
using NaughtyAttributes;
using UniRx;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UIElements;

namespace VRInput.Runtime
{
    public class HandPoseRecognizer : MonoBehaviour,
        IMixedRealityHandJointHandler{

        public HandPose pose => poseProvider?.GetPose();
        [NaughtyAttributes.Required]
        [SerializeField] HandPoseScriptable poseProvider;
       
        public HandPoseEventGenerator poseComparer => _poseComparer;
        [FormerlySerializedAs("poseComparer")]
        [SerializeReference] HandPoseEventGenerator _poseComparer;

        [SerializeField] MixedRealityInputAction inputAction;

        void OnValidate() {
            if (_poseComparer == null) {
                _poseComparer = new HandPoseEventGenerator();
            }
            _poseComparer.GetNewPoseComparerIfNeeded();
        }

        void OnEnable() {
            if (poseProvider == null) {
                enabled = false;
                return;
            }
            IMixedRealityInputSystem inputSystem = CoreServices.InputSystem;
            inputSystem.RegisterHandler<IMixedRealityHandJointHandler>(this);
            _poseComparer.recognized.Subscribe(SendToInputSystem);
        }

        void OnDisable() {
            CoreServices.InputSystem?.UnregisterHandler<IMixedRealityHandJointHandler>(this);
        }

        public void OnHandJointsUpdated(InputEventData<
            IDictionary<TrackedHandJoint, MixedRealityPose>> eventData) {
            _poseComparer.UpdateData(poseProvider, eventData);
        }

        void SendToInputSystem(HandPoseDetectionData gestureState) {
            IMixedRealityHand hand = HandJointUtils.FindHand(gestureState.hand);
            if (hand == null) {
                return;
            }
            if (gestureState.detected) {
                CoreServices.InputSystem.RaiseGestureStarted(
                    hand,
                    inputAction
                );
            }
            else {
                CoreServices.InputSystem.RaiseGestureCompleted(
                    hand,
                    inputAction
                );
            }
        }
    }
}