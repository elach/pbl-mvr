﻿using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.Utilities;
using Newtonsoft.Json;
using UnityEngine;

namespace VRInput.Runtime
{
    public class PoseRecorder : MonoBehaviour {

        [SerializeField] Handedness hand;
        [SerializeField] HandPoseScriptable scriptable;
        
        [NaughtyAttributes.Button]
        void DebugToPose() {
            IMixedRealityHand h = HandJointUtils.FindHand(hand);
            HandPose pose = new HandPose(h);
            if (scriptable != null) {
                scriptable.SavePose(pose);
            }
            
        }
    }
}