﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.Utilities;
using Newtonsoft.Json;
using UnityEngine;

namespace VRInput.Runtime
{
    [JsonObject(MemberSerialization.OptIn)]
    [Serializable]
    public class Finger {

        [JsonProperty]
        Dictionary<int, MixedRealityPose> stagesInternal;

        [JsonProperty]
        FingerType f;
        
        #region CONSTRUCTORS

        public Finger(FingerType type) {
            f = type;
            stagesInternal = new Dictionary<int, MixedRealityPose>();

            foreach (JointStage stage in Enum.GetValues(typeof(JointStage))) {
                FingerJointAddress addr =new FingerJointAddress(f, stage);
                if (addr.joint == TrackedHandJoint.None) {
                    continue;
                }
                stagesInternal.Add(addr.ToInt(),
                    new MixedRealityPose());
            }
        }

        public Finger(FingerType type, IMixedRealityHand hand)
            : this(type) {

            foreach (int addr in stagesInternal.Keys.ToList()) {
                MixedRealityPose mrPose;
                hand.TryGetJoint((new FingerJointAddress(addr)).joint, out mrPose);
                stagesInternal[addr] = mrPose;
            }
        }

        public Finger(FingerType type,
            IDictionary<TrackedHandJoint, MixedRealityPose> poses)
            : this(type) {

            foreach (var pair in poses) {
                FingerJointAddress addr = new FingerJointAddress(pair.Key);
                if (addr.finger == f) {
                    stagesInternal[addr.ToInt()] = pair.Value;
                }
            }

        }

        public Finger() {
            stagesInternal = null;
            f = 0;
        }
        
        [JsonConstructor]
        public Finger(Dictionary<int, MixedRealityPose> stagesInternal, FingerType f) {
            this.stagesInternal = stagesInternal;
            this.f = f;
        }

        #endregion CONSTRUCTORS


        public IDictionary<FingerJointAddress, MixedRealityPose> GetStages() {
            return stagesInternal.ToDictionary(
                pair => new FingerJointAddress(pair.Key),
                pair => pair.Value);
        }

    }
}