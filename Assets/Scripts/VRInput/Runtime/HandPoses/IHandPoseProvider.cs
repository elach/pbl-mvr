﻿using Microsoft.MixedReality.Toolkit.Input;

namespace VRInput.Runtime
{
    public interface IHandPoseProvider {
        HandPose GetPose();
        void SavePose(HandPose newPose);

        bool AllowBothHands();
    }
}