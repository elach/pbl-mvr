﻿using System.Collections.Generic;
using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.Utilities;
using UnityEngine;

namespace VRInput.Runtime {
    [System.Serializable]
    public class HandRotPoseComparer : IHandPoseComparer {
        
       [SerializeField] float normalizationFalloff = 1500.0f;
       float normalizationFactor => -1 / normalizationFalloff;
        
        public float PoseSimilarityFactor(IHandPoseProvider poseProvider,
            InputEventData<IDictionary<TrackedHandJoint, MixedRealityPose>> eventData) {
            
            HandPose pose = poseProvider.GetPose();
            var poseVal = pose.HandednessCheck(eventData, poseProvider.AllowBothHands());
            if (poseVal == HandPose.HandednessResult.NotCompatible) {
                return 0.0f;
            }
            float diffCumulative = 0;
            var dict = eventData.InputData;
            
            foreach (var pair in dict) {
                Quaternion reference;
                if (poseVal == HandPose.HandednessResult.Mirrored) {
                    reference = pose.GetMRPoseFlipped(pair.Key).Rotation;
                }
                else {
                    reference = pose.GetMRPose(pair.Key).Rotation;
                }
                Quaternion current = pair.Value.Rotation;
                Vector3 axis;
                float angle;
                Quaternion diff = Quaternion.Inverse(reference)*current;
                diff.ToAngleAxis(out angle, out axis);
                diffCumulative += angle;
            }

            float processed = DiffToPercent(diffCumulative);
            return processed;
        }

        float  DiffToPercent(float input) {
            return Mathf.Exp(normalizationFactor * input);
        }


    }
}