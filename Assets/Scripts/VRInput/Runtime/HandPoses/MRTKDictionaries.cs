﻿using System;
using Microsoft.MixedReality.Toolkit.Utilities;
using UnityEngine;

namespace VRInput.Runtime
{
    [Serializable]
    public class StringHandPoseDictionary : SerializableDictionary<string, HandPose> { }

    [Serializable]
    public class TrackedHandJointTransformDictionary : SerializableDictionary<TrackedHandJoint, Transform> { }
}