﻿using System.Collections.Generic;
using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.Utilities;
using NaughtyAttributes;
using UniRx;
using UnityEngine;
using UnityEngine.Serialization;

namespace VRInput.Runtime {
    [System.Serializable]
    public class HandPoseEventGenerator {
        [SerializeField] float recognitionTreshold = 0.7f;
        [ShowNonSerializedField] float lastRecognition;

        public IReadOnlyReactiveProperty<HandPoseDetectionData> recognized => _recognized;
        public IReadOnlyReactiveProperty<float> recognitionRemaining=> _recognitionRemaining;
        [ShowNonSerializedField]
        HandPoseDetectionDataReactiveProperty _recognized = new HandPoseDetectionDataReactiveProperty(
            new HandPoseDetectionData(false, Handedness.None));
        
        //say 0.5 means recognized
        //if 0.25 result then recognized = false, remaining = 0.25
        //if 0.75 result then recognized = true, remainig = 0;
        //if -0.5 result then recognized = false, remainig = 0.75;
        [ShowNonSerializedField]
        FloatReactiveProperty _recognitionRemaining = new FloatReactiveProperty(1.0f);




        enum PoseComparerImplementation { JustRotation }

        [SerializeReference] IHandPoseComparer comparer;
        [SerializeField] PoseComparerImplementation poseComparerSelector;

        public void GetNewPoseComparerIfNeeded() {
            IHandPoseComparer toReplace = EnumTypeMapping(poseComparerSelector);
            if (comparer == null || toReplace.GetType() != comparer.GetType()) {
                comparer = toReplace;
            }
        }
        
        IHandPoseComparer EnumTypeMapping(PoseComparerImplementation implEnum) {
            switch (implEnum) {
                case PoseComparerImplementation.JustRotation:
                    return new HandRotPoseComparer();
                default:
                    return null;
            }    
        }
        

        public void UpdateData(
                IHandPoseProvider pose, 
                InputEventData<IDictionary<TrackedHandJoint, MixedRealityPose>> eventData) {

            lastRecognition = comparer.PoseSimilarityFactor(pose, eventData);
            if (lastRecognition >= recognitionTreshold) {
                UpdateRecognition(true, null, eventData.Handedness);
            }
            else {
                UpdateRecognition(false, 
                    Mathf.Abs(recognitionTreshold - lastRecognition),
                    eventData.Handedness);
            }
        }

        void UpdateRecognition(bool rec, float? remaining, Handedness hand) {
            _recognized.Value = new HandPoseDetectionData(rec, hand);
            if (remaining.HasValue) {
                _recognitionRemaining.Value = remaining.Value;
            }
        }
    }


}