﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.Input;
using UnityEngine;

public class InitPlayer : MonoBehaviour {
    
    void Start() {
        MixedRealityPlayspace.Position = transform.position;
        MixedRealityPlayspace.Rotation = transform.rotation;

        OVRManager.display.displayFrequency = OVRManager.display.displayFrequenciesAvailable.ToList().Min(); 
        
        IMixedRealityGazeProvider gazeProvider =  MixedRealityToolkit.InputSystem?.GazeProvider;
        gazeProvider.GameObjectReference.AddComponent<SphereCollider>();
        Rigidbody rb = gazeProvider.GameObjectReference.AddComponent<Rigidbody>();
        rb.isKinematic = true;
    }

}
