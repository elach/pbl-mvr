﻿using UnityEngine;
 

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    private static object m_Lock = new object();
    private static T m_Instance;
    
    public static T Instance
    {
        get
        {
            lock (m_Lock)
            {
                if (m_Instance == null)
                {
                    m_Instance = (T)FindObjectOfType(typeof(T));
                    if (m_Instance == null) {
                        Debug.LogError($"No instance of {typeof(T).Name} found");
                    }
                }
 
                return m_Instance;
            }
        }
    }
    
}