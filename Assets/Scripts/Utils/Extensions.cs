﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

/// <summary>
/// Klasa rozszerzająca skrypty i komponenty Unity
/// </summary>
public static class Extensions {

    /// <summary>
    /// Metoda usuwająca wszystkie tranformy/objekty zawarte jako dzieci pod daną transformą
    /// </summary>
    /// <param name="parent"></param>
    public static void DestroyAllChildren(this Transform parent) {
        foreach (Transform child in parent) {
            GameObject.Destroy(child.gameObject);
        }
    }

    public static Vector3 YAs(this Vector3 v3, float y)
    {
        return new Vector3(v3.x, y, v3.z);
    }

    public static void AsIf<T>(this object value, Action<T> action) where T : class {
        T t = value as T;
        if (t != null) {
            action(t);
        }
    }

    public static void GetNotNullComponent<T>(this Component value, Action<T> action) where T : class {
        T t  =value.GetComponent<T>();
        if (t != null) {
            action(t);
        }
    }

    public static T RandomElement<T>(this IList<T> list) {
        return list.ElementAt(Random.Range(0, list.Count));
    }

    public static int[] RandomNonRepeatingIndexes<T>(this IList<T> list, int count) {
        int capacity = list.Count();
        int[] toSelect = new int[count];
        if (count >= capacity) {
            //Ensures we won't enter an infinite loop in a moment
            return list.Select(el => list.IndexOf(el)).ToArray();
        }
        for (int i = 0; i < count; i++) {
            do {
                toSelect[i] = Random.Range(0, capacity);
            } while (toSelect.Where(x => x == toSelect[i]).Count() != 1);
        }

        return toSelect;
    }

    public static IEnumerable<V> ZipDefaults<T, U, V>(this IEnumerable<T> first, IEnumerable<U> second, Func<T, U, V> op) {
        if (first.Count() < second.Count()) {
            return first.Concat(Enumerable.Repeat(default(T), second.Count() - first.Count()))
                .Zip(second, op);
        }
        else {

            return first.Zip(
                second.Concat(Enumerable.Repeat(default(U), first.Count() - second.Count())), op);
        }
    }
}