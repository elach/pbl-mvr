﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatchEnableObjects : MonoBehaviour {
    
    [SerializeField] GameObject[] gameObjects;
    [SerializeField] GameObject[] inverseGameObjects;

    void OnEnable() {
        Set(gameObjects, true);
        Set(inverseGameObjects, false);

    }

    void OnDisable() {
        Set(gameObjects, false);
        Set(inverseGameObjects, true);
    }

    void Set(IEnumerable<GameObject> enumerable, bool value) {
        foreach (GameObject o in enumerable) {
            if (o != null) {
                o.SetActive(value);    
            }
        }
    }
}
