﻿using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.Input;
using System.Collections.Generic;
using UnityEngine;

public class AfflictionPanelController : MonoBehaviour, IMixedRealityInputActionHandler
{
    /// <summary>
    /// Pole serializowane przechowujące informację o typie akcji włączającego panel wyboru schorzenia
    /// </summary>
    [Header("Input for Affliction Panel")]
    [SerializeField] private MixedRealityInputAction openAfflictionMenuAction;
    /// <summary>
    /// Pole serializowane przechowujące referencję na obiekt AfflictionController
    /// </summary>
    [Header("Affliction Panel Settings")]
    [SerializeField] private AfflictionController afflictionController;
    /// <summary>
    /// Pole serializowane przechowujące referencję na Gameobject panelu wyboru schorzeń 
    /// </summary>
    [SerializeField] private GameObject afflictionPanel;
    /// <summary>
    /// Pole serializowane przechowujące referencję na Gameobject przycisku ustawień schorzenia 
    /// </summary>
    [SerializeField] private GameObject settingsButton;
    /// <summary>
    /// Lista serializowana zawierająca wszystkie okna ustawień schorzeń
    /// </summary>
    [SerializeField] List<AfflictionSettingsWindow> afflictionSettingsWindows;

    private AfflictionSettingsWindow currentSettingsWindowToOpen;

    private void Awake()
    {
        afflictionController.InitializeAfflictionController();

        foreach (var window in afflictionSettingsWindows)
        {
            window.InitializeSettingsWindow();
            window.ToggleAfflictionSettingsWindow(false);
        }

        ToggleAfflictionPanel(false);
        ToggleSettingsButton(AfflictionType.Default);
    }

    private void OnEnable()
    {
        AfflictionSettingsWindow.OnToggleMainPanel += ToggleAfflictionPanel;
        AfflictionButton.OnAfflictionChange += ToggleSettingsButton;
        CoreServices.InputSystem?.RegisterHandler<IMixedRealityInputActionHandler>(this);
    }

    private void OnDisable()
    {
        AfflictionSettingsWindow.OnToggleMainPanel -= ToggleAfflictionPanel;
        AfflictionButton.OnAfflictionChange -= ToggleSettingsButton;
        CoreServices.InputSystem?.UnregisterHandler<IMixedRealityInputActionHandler>(this);
    }

    /// <summary>
    /// Metoda włączająca lub wyłączająca panel wyboru schorzeń w zależności od parametru enabled
    /// </summary>
    /// <param name="enabled">Jeżeli true to panel zostanie włączony. Jeśli false panel zostanie wyłączony</param>
    public void ToggleAfflictionPanel(bool enabled)
    {
        if (enabled)
        {
            var gazeCursor = CoreServices.InputSystem.GazeProvider.GazeCursor;
            afflictionPanel.transform.position = gazeCursor.Position;
        }
        afflictionPanel.SetActive(enabled);
    }

    /// <summary>
    /// Metoda wyowłująca się na użyciu przycisku ustawień schorzenia
    /// </summary>
    public void OnSettingsButtonClick()
    {
        currentSettingsWindowToOpen.ToggleAfflictionSettingsWindow(true);
        ToggleAfflictionPanel(false);
    }

    public void OnActionStarted(BaseInputEventData eventData)
    {
        if (eventData.MixedRealityInputAction.Id == openAfflictionMenuAction.Id)
        {
            if (!afflictionPanel.activeSelf)
            {
                ToggleAfflictionPanel(true);
            }
        }
    }

    public void OnActionEnded(BaseInputEventData eventData)
    {
    }

    /// <summary>
    /// Metoda włączająca lub wyłączająca przycisk ustawień schorzenia
    /// </summary>
    /// <param name="afflictionType">Jeśli true to przycisk będzie widoczny. Jeśli false przycisk będzie niewidoczny</param>
    private void ToggleSettingsButton(AfflictionType afflictionType)
    {
        AfflictionSettingsWindow window = afflictionSettingsWindows.Find(x => x.AfflictionData.AfflictionType == afflictionType);
        if (window != null)
        {
            settingsButton.SetActive(true);
            currentSettingsWindowToOpen = window;
        }
        else
        {
            settingsButton.SetActive(false);
            currentSettingsWindowToOpen = null;
        }
    }

}
