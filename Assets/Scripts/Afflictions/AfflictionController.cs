﻿using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.Input;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

/// <summary>
/// Klasa implementująca logikę zmiany schorzeń
/// </summary>
public class AfflictionController : MonoBehaviour, IMixedRealityInputActionHandler
{
    /// <summary>
    /// Pole serializowane przechowujące informację o typie akcji zmieniającego globalnie schorzenie
    /// </summary>
    [Header("Input for global disable ")]
    [SerializeField] private MixedRealityInputAction globalAfflictionToggleAction;
    /// <summary>
    /// Serializowana Lista schorzeń
    /// </summary>
    [SerializeField] private List<AfflictionData> afflictions = new List<AfflictionData>();
    /// <summary>
    /// Zmienna przechowujące referencję na obecne schorzenie
    /// </summary>
    private AfflictionData currentAffliction;
    /// <summary>
    /// Zmienna przechowujące referencję na obecne schorzenie
    /// </summary>
    private AfflictionData previousAffliction;

    private void OnEnable()
    {
        AfflictionButton.OnAfflictionChange += ChangeAffliction;
        CoreServices.InputSystem?.RegisterHandler<IMixedRealityInputActionHandler>(this);
    }

    private void OnDisable()
    {
        AfflictionButton.OnAfflictionChange -= ChangeAffliction;
        CoreServices.InputSystem?.UnregisterHandler<IMixedRealityInputActionHandler>(this);

    }

    /// <summary>
    /// Metoda inicjalizująca klasę
    /// </summary>
    public void InitializeAfflictionController()
    {
        ChangeAffliction(AfflictionType.Default);
    }

    public void OnActionStarted(BaseInputEventData eventData)
    {
        ToggleAfflictionGlobaly(eventData);
    }

    public void OnActionEnded(BaseInputEventData eventData)
    {
    }

    /// <summary>
    /// Metoda implementująca logikę zmiany schorzenia
    /// </summary>
    /// <param name="afflictionType">typ schorzenia do zmiany</param>
    private void ChangeAffliction(AfflictionType afflictionType)
    {
        AfflictionData affliction = afflictions.Find(x => x.AfflictionType == afflictionType);
        if (affliction != currentAffliction)
        {
            previousAffliction = currentAffliction;
            currentAffliction = affliction;
            GraphicsSettings.renderPipelineAsset = currentAffliction.RenderPipeline;
            QualitySettings.renderPipeline = currentAffliction.RenderPipeline;
        }
    }

    /// <summary>
    /// Metoda implementująca logike włączania i wyłączania schorzenia w dowolnym momencie
    /// </summary>
    /// <param name="eventData">dane akcji kontrolera</param>
    private void ToggleAfflictionGlobaly(BaseInputEventData eventData)
    {
        if (eventData.MixedRealityInputAction.Id == globalAfflictionToggleAction.Id && previousAffliction != null)
        {
            AfflictionType affliction = currentAffliction.AfflictionType != AfflictionType.Default ?
                AfflictionType.Default : previousAffliction.AfflictionType;
            ChangeAffliction(affliction);
        }
    }
}


