﻿/// <summary>
/// Typ enum opisujacy schorzenie
/// </summary>
public enum AfflictionType
{
    Cataract = 1,
    Glaucoma = 2,
    OldSightedness = 3,
    YellowEye = 4,
    Default = 5
}
