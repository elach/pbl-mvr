using UnityEngine.Rendering.Universal;

namespace UnityEngine.Experiemntal.Rendering.Universal
{
    /// <summary>
    /// Klasa implementująca Renderer Feature Blit
    /// </summary>
    public class Blit : ScriptableRendererFeature
    {
        /// <summary>
        /// Klasa serializowana przechowująca dane Renderer Feature'a
        /// </summary>
        [System.Serializable]
        public class BlitSettings
        {
            /// <summary>
            /// Zmienna przechowująca referencję na typ zdarzenia renderowania 
            /// </summary>
            public RenderPassEvent Event = RenderPassEvent.AfterRenderingOpaques;
            /// <summary>
            /// Zmienna przechowująca referencję na materiał
            /// </summary>
            public Material BlitMaterial = null;
            /// <summary>
            /// Zmienna przechowująca referencję na indeks przebiegu
            /// </summary>
            public int BlitMaterialPassIndex = -1;
            public Target Destination = Target.Color;
            public string TextureId = "_BlitPassTexture";
        }

        /// <summary>
        /// enum charakteryzujący destynację renderowania
        /// </summary>
        public enum Target
        {
            Color,
            Texture
        }

        /// <summary>
        /// Zmienna przechowująca referencję na ustawienia Renderer Feature'a
        /// </summary>
        public BlitSettings settings = new BlitSettings();
        RenderTargetHandle m_RenderTextureHandle;

        /// <summary>
        /// Zmienna klasy blitPass
        /// </summary>
        BlitPass blitPass;

        /// <inheritdoc/>
        public override void Create()
        {
            var passIndex = settings.BlitMaterial != null ? settings.BlitMaterial.passCount - 1 : 1;
            settings.BlitMaterialPassIndex = Mathf.Clamp(settings.BlitMaterialPassIndex, -1, passIndex);
            blitPass = new BlitPass(settings.Event, settings.BlitMaterial, settings.BlitMaterialPassIndex, name);
            m_RenderTextureHandle.Init(settings.TextureId);
        }

        /// <inheritdoc/>
        public override void AddRenderPasses(ScriptableRenderer renderer, ref RenderingData renderingData)
        {
            var src = renderer.cameraColorTarget;
            var dest = (settings.Destination == Target.Color) ? RenderTargetHandle.CameraTarget : m_RenderTextureHandle;

            if (settings.BlitMaterial == null)
            {
                Debug.LogWarningFormat("Missing Blit Material. {0} blit pass will not execute. Check for missing reference in the assigned renderer.", GetType().Name);
                return;
            }

            blitPass.Setup(src, dest);
            renderer.EnqueuePass(blitPass);
        }
    }
}
