﻿using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

namespace UnityEngine.Experiemntal.Rendering.Universal
{
    /// <summary>
    /// Klasa implementująca Blit pass
    /// </summary>
    public class BlitPass : ScriptableRenderPass
    {
        /// <summary>
        /// enum charakteryzujący destynację renderowania
        /// </summary>
        public enum RenderTarget
        {
            Color,
            RenderTexture,
        }

        /// <summary>
        /// zmienna przechowująca referencję na materiał
        /// </summary>
        public Material blitMaterial = null;
        /// <summary>
        /// zmienna przechowująca referencję na indeks przebiegu 
        /// </summary>
        public int blitShaderPassIndex = 0;
        public FilterMode filterMode { get; set; }
        /// <summary>
        /// Właściwość przechowująca referencję na strukturą źródłową
        /// </summary>
        private RenderTargetIdentifier source { get; set; }
        /// <summary>
        /// Właściwość przechowująca referencję na strukturą docelową
        /// </summary>
        private RenderTargetHandle destination { get; set; }

        private RenderTargetHandle m_TemporaryColorTexture;
        private string m_ProfilerTag;

        /// <summary>
        /// Tworzenie przebiegu
        /// </summary>
        public BlitPass(RenderPassEvent renderPassEvent, Material blitMaterial, int blitShaderPassIndex, string tag)
        {
            this.renderPassEvent = renderPassEvent;
            this.blitMaterial = blitMaterial;
            this.blitShaderPassIndex = blitShaderPassIndex;
            m_ProfilerTag = tag;
            m_TemporaryColorTexture.Init("_TemporaryColorTexture");
        }

        /// <summary>
        /// Konfiguracja przebiegu przy pomocy struktury źródłowej i docelowej
        /// </summary>
        /// <param name="source">zmienna przechowująca referencję na strukturą źródłową</param>
        /// <param name="destination">zmienna przechowująca referencję na strukturą docelową</param>
        public void Setup(RenderTargetIdentifier source, RenderTargetHandle destination)
        {
            this.source = source;
            this.destination = destination;
        }

        /// <inheritdoc/>
        public override void Execute(ScriptableRenderContext context, ref RenderingData renderingData)
        {
            CommandBuffer cmd = CommandBufferPool.Get(m_ProfilerTag);

            RenderTextureDescriptor opaqueDesc = renderingData.cameraData.cameraTargetDescriptor;
            opaqueDesc.depthBufferBits = 0;

            // Can't read and write to same color target, create a temp render target to blit. 
            if (destination == RenderTargetHandle.CameraTarget)
            {
                cmd.GetTemporaryRT(m_TemporaryColorTexture.id, opaqueDesc, filterMode);
                Blit(cmd, source, m_TemporaryColorTexture.Identifier(), blitMaterial, blitShaderPassIndex);
                Blit(cmd, m_TemporaryColorTexture.Identifier(), source);
            }
            else
            {
                Blit(cmd, source, destination.Identifier(), blitMaterial, blitShaderPassIndex);
            }

            context.ExecuteCommandBuffer(cmd);
            CommandBufferPool.Release(cmd);
        }

        /// <inheritdoc/>
        public override void FrameCleanup(CommandBuffer cmd)
        {
            if (destination == RenderTargetHandle.CameraTarget)
                cmd.ReleaseTemporaryRT(m_TemporaryColorTexture.id);
        }
    }
}