﻿using UnityEngine;
using UnityEngine.Rendering.Universal;

[CreateAssetMenu(fileName = "Affliction", menuName = "Affliction", order = 1)]

/// <summary>
/// Klasa implementująca kontener przechowujący dane związane ze schorzeniem
/// </summary>
public class AfflictionData : ScriptableObject
{
    /// <summary>
    /// Pole zmienna przechowując informację o typie schorzenia
    /// </summary>
    public AfflictionType AfflictionType;
    /// <summary>
    /// Materiał stworzony na podstawie shadera implementującego post-process
    /// </summary>
    public Material AfflictionMaterial;
    /// <summary>
    /// Pole przechowujące referencję na render pipeline asset 
    /// </summary>
    public UniversalRenderPipelineAsset RenderPipeline;
}
