﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Klasa implementująca zachwowanie przycisku charakteryzującego schorzenie
/// </summary>
public class AfflictionButton : MonoBehaviour
{
    /// <summary>
    /// Pole serializowane przechowujące referencję na typ schorzenia jakie charakteryzuje przycisk
    /// </summary>
    [SerializeField]
    private AfflictionType afflictionType;

    /// <summary>
    /// statyczne zdarzenie typu AfflictionType wywoływane w momencie użycia przycisku
    /// </summary>
    public static Action<AfflictionType> OnAfflictionChange;

    /// <summary>
    /// Metoda wywoływana w momencie użycia przycisku 
    /// </summary>
    public void OnAfflictionButtonClick()
    {
        OnAfflictionChange.Invoke(afflictionType);
    }
}
