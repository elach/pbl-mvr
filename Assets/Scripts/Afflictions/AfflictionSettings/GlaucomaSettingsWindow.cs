﻿using Microsoft.MixedReality.Toolkit.UI;
using UnityEngine;

public class GlaucomaSettingsWindow : AfflictionSettingsWindow
{
    /// <summary>
    /// Zmienna przechowująca referencję na suwak Radius
    /// </summary>
    [Header("Glaucoma Settings Elements")]
    [SerializeField] private PinchSlider radiusSlider; // 0 - 2.5 def 1 name _Radius
    /// <summary>
    /// Zmienna przechowująca referencję na suwak Fade
    /// </summary>
    [SerializeField] private PinchSlider fadeSlider; // 0 - 5 def 2 name _Fade

    protected override void InitializeSettings()
    {
        radiusSlider.SetSliderValue(AfflictionData.AfflictionMaterial.GetFloat("_Radius"));
        fadeSlider.SetSliderValue(AfflictionData.AfflictionMaterial.GetFloat("_Fade"));
    }

    protected override void InitializeDefaultMaterialSettings()
    {
        AfflictionData.AfflictionMaterial.SetFloat("_Radius", 1);
        AfflictionData.AfflictionMaterial.SetFloat("_Fade", 2);
    }

    protected override void SetUpUIElements()
    {
        radiusSlider.SetSliderMaxValue(2.5f);
        fadeSlider.SetSliderMaxValue(5f);
    }

    /// <summary>
    /// Metoda wywołująca się w momencie zmiany wartości dla suwaka Radius
    /// </summary>
    public void OnRadiusSliderChange()
    {
        AfflictionData.AfflictionMaterial.SetFloat("_Radius", radiusSlider.GetSliderValue());
    }

    /// <summary>
    /// Metoda wywołująca się w momencie zmiany wartości dla suwaka Fade
    /// </summary>
    public void OnFadeSliderChange()
    {
        AfflictionData.AfflictionMaterial.SetFloat("_Fade", fadeSlider.GetSliderValue());
    }
}
