﻿using UnityEngine;
using System;
using Microsoft.MixedReality.Toolkit;

/// <summary>
/// Klasa abstrakcyjna implementująca zachowanie i sterowanie oknem ustawień
/// </summary>
public abstract class AfflictionSettingsWindow : MonoBehaviour
{
    /// <summary>
    /// Zmienna przechowująca referencję na ScriptableObject z danymi o schorzeniu
    /// </summary>
    [SerializeField]
    private AfflictionData afflictionData;

    /// <summary>
    /// Zdarzenie które wywołane wywoła metodę aktywującaą lub dezaktywującą główny panel
    /// </summary>
    public static Action<bool> OnToggleMainPanel;

    /// <summary>
    /// Właściwość zwracająca zmienną afflictionData
    /// </summary>
    public AfflictionData AfflictionData => afflictionData;

    /// <summary>
    /// Metoda inicjalizująca okno ustawień
    /// </summary>
    public void InitializeSettingsWindow()
    {
        SetUpUIElements();
        InitializeDefaultMaterialSettings();
    }

    /// <summary>
    /// Metoda odpowiadająca za aktywacje lub dezaktywację okna ustawień
    /// </summary>
    /// <param name="enabled">Jeśli true okno ustawień otworzy się i wywoła metodę InitializeSettings().
    /// Jeśli false okno ustwień zamknie się a panel główny otworzy się.
    /// </param>
    public void ToggleAfflictionSettingsWindow(bool enabled)
    {
        if (enabled)
        {
            InitializeSettings();
            var gazeCursor = CoreServices.InputSystem.GazeProvider.GazeCursor;
            gameObject.transform.position = gazeCursor.Position;
        }
        else
        {
            OnToggleMainPanel?.Invoke(true);
        }
        gameObject.SetActive(enabled);
    }

    /// <summary>
    /// Metoda inicjalizująca elementy w oknie ustawień
    /// </summary>
    protected abstract void InitializeSettings();
    /// <summary>
    /// Metoda inicjalizująca domyślne wartości dla materiałów schorzeń
    /// </summary>
    protected abstract void InitializeDefaultMaterialSettings();
    /// <summary>
    /// Metoda inizjalizująca i ustawiająca wartości dla elementów interfejsu
    /// </summary>
    protected abstract void SetUpUIElements();

}
