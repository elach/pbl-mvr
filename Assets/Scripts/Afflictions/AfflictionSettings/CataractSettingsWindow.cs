﻿using Microsoft.MixedReality.Toolkit.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CataractSettingsWindow : AfflictionSettingsWindow
{
    /// <summary>
    /// Zmienna przechowująca referencję na suwak BlurSize
    /// </summary>
    [Header("Blur Settings Elements")]
    [SerializeField] private PinchSlider blurSizeSlider; // 0 - 0.1 def 0  name_ BlurSize
    /// <summary>
    /// Zmienna przechowująca referencję na suwak Samples
    /// </summary>
    [SerializeField] private PinchSlider blurSamplesSlider; // 0 - 100 def 30/60 name _BlurSamples
    /// <summary>
    /// Zmienna przechowująca referencję na suwak standardDeviation
    /// </summary>
    [SerializeField] private PinchSlider standardDeviationSlider; // 0 - 0.3 def 0.02 name _StandardDeviation

    /// <summary>
    /// Zmienna przechowująca referencję na suwak hueShift
    /// </summary>
    [Header("Whitening Settings Elements")]
    [SerializeField] private PinchSlider hueShiftSlider; // 0 - 1 def 1 name _HueShift
    /// <summary>
    /// Zmienna przechowująca referencję na suwak Saturation
    /// </summary>
    [SerializeField] private PinchSlider saturationSlider; // 0 - 1 def 1 name _SaturationPower
    /// <summary>
    /// Zmienna przechowująca referencję na suwak Brightness
    /// </summary>
    [SerializeField] private PinchSlider brightnessSlider; // 0 - 1 def 1 name _ValuePower

    /// <summary>
    /// Zmienna przechowująca referencję na suwak Intensity
    /// </summary>
    [Header("Lambency Settings Elements")]
    [SerializeField] private PinchSlider lambencyIntensitySlider; // 0 - 10 def 1 name _LambencyIntensity

    protected override void InitializeSettings()
    {
        blurSizeSlider.SetSliderValue(AfflictionData.AfflictionMaterial.GetFloat("_BlurSize"));
        blurSamplesSlider.SetSliderValue(AfflictionData.AfflictionMaterial.GetFloat("_BlurSamples"));
        standardDeviationSlider.SetSliderValue(AfflictionData.AfflictionMaterial.GetFloat("_StandardDeviation"));
        hueShiftSlider.SetSliderValue(AfflictionData.AfflictionMaterial.GetFloat("_HueShift"));
        saturationSlider.SetSliderValue(AfflictionData.AfflictionMaterial.GetFloat("_SaturationPower"));
        brightnessSlider.SetSliderValue(AfflictionData.AfflictionMaterial.GetFloat("_ValuePower"));
        lambencyIntensitySlider.SetSliderValue(AfflictionData.AfflictionMaterial.GetFloat("_LambencyIntensity"));
    }

    protected override void InitializeDefaultMaterialSettings()
    {
        AfflictionData.AfflictionMaterial.SetFloat("_BlurSize", 0.02f);
        AfflictionData.AfflictionMaterial.SetFloat("_BlurSamples", 15f);
        AfflictionData.AfflictionMaterial.SetFloat("_StandardDeviation", 0.02f);
        AfflictionData.AfflictionMaterial.SetFloat("_HueShift", 1f);
        AfflictionData.AfflictionMaterial.SetFloat("_SaturationPower", 0.775f);
        AfflictionData.AfflictionMaterial.SetFloat("_ValuePower", 1f);
        AfflictionData.AfflictionMaterial.SetFloat("_LambencyIntensity", 1.5f);
    }

    protected override void SetUpUIElements()
    {
        blurSizeSlider.SetSliderMaxValue(0.1f);
        blurSamplesSlider.SetSliderMaxValue(60f);
        standardDeviationSlider.SetSliderMaxValue(0.3f);
        hueShiftSlider.SetSliderMaxValue(1f);
        saturationSlider.SetSliderMaxValue(1f);
        brightnessSlider.SetSliderMaxValue(1f);
        lambencyIntensitySlider.SetSliderMaxValue(10f);
    }

    /// <summary>
    /// Metoda wywołująca się w momencie zmiany wartości dla suwaka BlurSize
    /// </summary>
    public void OnBlurSizeChanged()
    {
        AfflictionData.AfflictionMaterial.SetFloat("_BlurSize", blurSizeSlider.GetSliderValue());
    }

    /// <summary>
    /// Metoda wywołująca się w momencie zmiany wartości dla suwaka BlurSamples
    /// </summary>
    public void OnBlurSamplesChanged()
    {
        AfflictionData.AfflictionMaterial.SetFloat("_BlurSamples", blurSamplesSlider.GetSliderValue());
    }

    /// <summary>
    /// Metoda wywołująca się w momencie zmiany wartości dla suwaka StandardDeviation
    /// </summary>
    public void OnStandardDeviationChanged()
    {
        AfflictionData.AfflictionMaterial.SetFloat("_StandardDeviation", standardDeviationSlider.GetSliderValue());
    }

    /// <summary>
    /// Metoda wywołująca się w momencie zmiany wartości dla suwaka HueShift
    /// </summary>
    public void OnHueShiftChanged()
    {
        AfflictionData.AfflictionMaterial.SetFloat("_HueShift", hueShiftSlider.GetSliderValue());
    }

    /// <summary>
    /// Metoda wywołująca się w momencie zmiany wartości dla suwaka Saturation
    /// </summary>
    public void OnSaturationChanged()
    {
        AfflictionData.AfflictionMaterial.SetFloat("_SaturationPower", saturationSlider.GetSliderValue());
    }

    /// <summary>
    /// Metoda wywołująca się w momencie zmiany wartości dla suwaka Brightness
    /// </summary>
    public void OnBrightnessChanged()
    {
        AfflictionData.AfflictionMaterial.SetFloat("_ValuePower", brightnessSlider.GetSliderValue());
    }

    /// <summary>
    /// Metoda wywołująca się w momencie zmiany wartości dla suwaka Intensity
    /// </summary>
    public void OnLambencyIntensityChanged()
    {
        AfflictionData.AfflictionMaterial.SetFloat("_LambencyIntensity", lambencyIntensitySlider.GetSliderValue());
    }
}
