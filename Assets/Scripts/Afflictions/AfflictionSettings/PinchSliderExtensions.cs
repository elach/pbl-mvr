﻿using Microsoft.MixedReality.Toolkit.UI;

/// <summary>
/// Statyczna klasa implementująca metody rozszerzeń dla klasy PinchSlider
/// </summary>
public static class PinchSliderExtensions
{
    /// <summary>
    /// Metoda ustawiająca maksymalną wartość suwaka
    /// </summary>
    /// <param name="slider">referencja na klasę suwaka</param>
    /// <param name="maxSliderValue">maksymalna wartość suwaka</param>
    public static void SetSliderMaxValue(this PinchSlider slider, float maxSliderValue)
    {
        slider.MaxSliderValue = maxSliderValue;
    }

    /// <summary>
    /// Metoda pobierająca aktualną wartość suwaka
    /// </summary>
    /// <param name="slider">referencja na klasę suwaka</param>
    /// <returns></returns>
    public static float GetSliderValue(this PinchSlider slider)
    {
        return slider.SliderValue * slider.MaxSliderValue;
    }

    /// <summary>
    /// Metoda ustawiająca aktualną wartość suwaka
    /// </summary>
    /// <param name="slider">referencja na klasę suwaka</param>
    /// <param name="valueToSet">wartość do ustawienia</param>
    public static void SetSliderValue(this PinchSlider slider, float valueToSet)
    {
        slider.SliderValue = valueToSet / slider.MaxSliderValue;
    }
}
