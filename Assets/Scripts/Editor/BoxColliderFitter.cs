﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using Microsoft.MixedReality.Toolkit;
using UnityEditorInternal;

public class BoxColliderFitter  {
     
    [MenuItem("Tools/Collider/Fit to Children")]
    static void FitToChildren() {
        foreach (GameObject rootGameObject in Selection.gameObjects) {
            BoxCollider collider = rootGameObject.GetComponent<BoxCollider>();
            if (collider == null) {
                collider = rootGameObject.AddComponent<BoxCollider>();
            }

            Bounds bounds = new Bounds();
            bool boundsSet = false;
            foreach (Renderer r in rootGameObject.GetComponentsInChildren<Renderer>()) {
                if (boundsSet ) {
                    bounds.Encapsulate(r.bounds);
                }
                else {
                    bounds = r.bounds;
                    boundsSet = true;
                }
            }

            if (!boundsSet) {
                continue;
            }
            
            Undo.RecordObject(collider, "Setting collider bounds");

            collider.center = bounds.center - rootGameObject.transform.position;
            collider.size = bounds.size;

        }
    }
     
}