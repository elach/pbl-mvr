﻿using UnityEngine;

public class WheelChairController : MonoBehaviour
{

    private static WheelChairController _instance;
    public static WheelChairController instance { get { return _instance; } }

    private void Awake()
    {
        if(_instance != null && _instance != this)
        {
            Debug.LogWarning("Two wheelchair controllers!");
        }
        else
        {
            _instance = this;
        }
    }

    enum SteerMode { motionDebug, electricOnly, normal, standing, none }

    [SerializeField] private Transform cam;
    [SerializeField] private GameObject wheelchair;
    [SerializeField] private Rigidbody capsuleRb;
    [SerializeField] private WheelCollider leftWheel;
    [SerializeField] private WheelCollider rightWheel;
    [SerializeField] private Collider leftWheelBox, rightWheelBox;
    [SerializeField] private float force, moveSpeed, minVelocity;
    [SerializeField] SteerMode mode;
    

    private bool standing = false;


    private void Update()
    {
        OVRInput.Update();
    }

    void FixedUpdate()
    {
        OVRInput.FixedUpdate();

        wheelchair.SetActive(!standing);

        standing = false;

        capsuleRb.constraints = capsuleRb.constraints & ~(RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezePositionY);

        switch (mode)
        {
            case SteerMode.motionDebug:
                MotionDebug();
                break;
            case SteerMode.electricOnly:
                ElectricOnly();
                break;
            case SteerMode.normal:
                NormalSteering();
                break;
            case SteerMode.standing:
                Standing();
                break;
        }
        /*
        if(capsuleRb.velocity.sqrMagnitude < minVelocity*minVelocity)
        {
            capsuleRb.velocity = Vector3.zero;
            
        }*/
    }

    void MotionDebug()
    {
        leftWheel.motorTorque = GetInputWithAlternate(Input.GetAxisRaw("Oculus_CrossPlatform_PrimaryThumbstickVertical"), Input.GetAxisRaw("UpDownLeft")) * force;
        rightWheel.motorTorque = GetInputWithAlternate(Input.GetAxisRaw("Oculus_CrossPlatform_SecondaryThumbstickVertical"), Input.GetAxisRaw("UpDownRight")) * force;
    }

    void ElectricOnly()
    {
        float vertical = GetInputWithAlternate(Input.GetAxisRaw("Oculus_CrossPlatform_SecondaryThumbstickVertical"), Input.GetAxisRaw("Vertical"));
        float horizontal = GetInputWithAlternate(Input.GetAxisRaw("Oculus_CrossPlatform_SecondaryThumbstickHorizontal"), Input.GetAxisRaw("Horizontal"));

        leftWheel.motorTorque = Mathf.Clamp(vertical + horizontal * Mathf.Sign(vertical), -1, 1) * force;
        rightWheel.motorTorque = Mathf.Clamp(vertical + -horizontal * Mathf.Sign(vertical), -1, 1) * force;
    }

    void NormalSteering()
    {
        
        if (OVRInput.Get(OVRInput.Button.PrimaryHandTrigger) && leftWheelBox.bounds.Contains(this.transform.GetChild(0).GetChild(0).GetChild(0).transform.TransformPoint(OVRInput.GetLocalControllerPosition(OVRInput.Controller.LHand))))
        {
            leftWheel.motorTorque = GetInputWithAlternate(OVRInput.GetLocalControllerVelocity(OVRInput.Controller.LHand).z, Input.GetAxisRaw("UpDownLeft")) * force;
        }

        if (OVRInput.Get(OVRInput.Button.SecondaryHandTrigger) && rightWheelBox.bounds.Contains(this.transform.GetChild(0).GetChild(0).GetChild(0).transform.TransformPoint(OVRInput.GetLocalControllerPosition(OVRInput.Controller.RHand))))
        {
            rightWheel.motorTorque = GetInputWithAlternate(OVRInput.GetLocalControllerVelocity(OVRInput.Controller.RHand).z, Input.GetAxisRaw("UpDownRight")) * force;
        }
    }

    void Standing()
    {
        standing = true;

        capsuleRb.constraints = capsuleRb.constraints | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezePositionY;
        if (OVRInput.Get(OVRInput.Button.PrimaryHandTrigger) && OVRInput.Get(OVRInput.Button.SecondaryHandTrigger)) {
            ApplyForwardMotion(OVRInput.GetLocalControllerVelocity(OVRInput.Controller.LHand), OVRInput.GetLocalControllerVelocity(OVRInput.Controller.RHand));
        }
    }

    public void ApplyForwardMotion(Vector3 leftHandVelocity, Vector3 rightHandVelocity) {
        capsuleRb.velocity = Quaternion.Euler(Vector3.up * cam.rotation.eulerAngles.y) *
                             (Vector3.forward *
                              Mathf.Max(
                                  (-leftHandVelocity.z + leftHandVelocity.y) * moveSpeed * Time.fixedDeltaTime,
                                  (rightHandVelocity.z + rightHandVelocity.y) * moveSpeed * Time.fixedDeltaTime, 
                                  minVelocity
                              )
                             );
    }



    private float GetInputWithAlternate(float mainInput, float alternateInput)
    {
        float result;
        result = mainInput != 0 ? mainInput : alternateInput;
        return result;
    }


}