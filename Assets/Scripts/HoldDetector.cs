﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.Utilities;
using UnityEngine;
using UniRx;

public class HoldDetector : MonoBehaviour, IMixedRealityInputActionHandler
{
    [SerializeField] MixedRealityInputAction grabAction;
    [SerializeField] WheelChairController controller;
    [SerializeField] float multiplier = 1.0f;
    
    bool handHoldLeft;
    bool handHoldRight;

    void Start() {
        CoreServices.InputSystem.RegisterHandler<IMixedRealityInputActionHandler>(this);
        
        Observable.EveryUpdate().Where((_) => handHoldLeft && handHoldRight).Subscribe((_) => {
            IMixedRealityController leftC = GetController(Handedness.Left);
            IMixedRealityController rightC = GetController(Handedness.Right);
            if (leftC == null) {
                handHoldLeft = false;
            }
            if (rightC == null) {
                handHoldRight = false;
            }
            if (!(handHoldLeft && handHoldRight)) {
                return;
            }
            controller.ApplyForwardMotion(leftC.Velocity * multiplier, rightC.Velocity * multiplier);
        }).AddTo(this);
    }

    void OnDestroy() {
        CoreServices.InputSystem?.UnregisterHandler<IMixedRealityInputActionHandler>(this);
    }

    public void OnActionStarted(BaseInputEventData eventData) {
        SetActive(eventData, true);
    }

    public void OnActionEnded(BaseInputEventData eventData) {
        SetActive(eventData, false);
    }

    void SetActive(BaseInputEventData eventData, bool enable) {
        if (grabAction.Id == eventData.MixedRealityInputAction.Id) {
            Handedness hand = GetHandedness(eventData);
            if ((hand & Handedness.Left) != 0) {
                handHoldLeft = enable;
            }          
            if ((hand & Handedness.Right) != 0) {
                handHoldRight = enable;
            }
        }
    }

    IMixedRealityController GetController(Handedness h) {
        return CoreServices.InputSystem.DetectedControllers.FirstOrDefault(c =>
            (c.ControllerHandedness & h) != 0);
    }
    
    Handedness GetHandedness(BaseInputEventData eventData) {
        IMixedRealityPointer[] pointers = eventData.InputSource.Pointers;
        if (pointers == null || pointers.Length == 0) {
            return Handedness.None;
        }
        else {
            return pointers[0].Controller.ControllerHandedness;
        }
    }
}
 