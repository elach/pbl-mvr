﻿using System;
using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Microsoft.MixedReality.Toolkit.UI;
using Microsoft.MixedReality.Toolkit.Utilities;
using UnityEngine;
using UnityEngine.Serialization;

/// <summary>
/// Klasa przechowująca informacje o obiekcie
/// </summary>

[SelectionBase]
public class FindableItemInfo : MonoBehaviour {

    public CollectableItems itemType;
    
    public AudioClip clip;
    [SerializeField] AudioClip correct;
    [SerializeField] AudioClip incorrect;
    [SerializeField] Interactable interactable;

    [SerializeField] AudioSource audioSource;

    public bool cannotTouch {
        set {
            mCannotTouch = value;
            if (mCannotTouch) {
                interactable.enabled = !cannotTouch;
            }
        }
        get {
            return mCannotTouch;
        }
    }

    bool mCannotTouch;

    /// <summary>
    /// Miejsce w którym został ukryty przedmiot
    /// </summary>
    [NonSerialized] public PlaceInfo place;

    [SerializeField] Material correctOutline;
    [SerializeField] Material incorrectOutline;
    bool markedCorrect;

    void Awake() {
        interactable.enabled = !cannotTouch;
        interactable.OnClick.AddListener(RaiseClickedEvent);
        interactable.AddReceiver<InteractableOnFocusReceiver>().OnFocusOn.AddListener(RaiseHoverEvent); 
    }

    void OnDestroy() {
        if (interactable != null) {
            interactable.OnClick.RemoveListener(RaiseClickedEvent);
            interactable.GetReceiver<InteractableOnFocusReceiver>().OnFocusOn.RemoveListener(RaiseHoverEvent); 
        }
    }

     void RaiseClickedEvent() {
        GamesManager.Instance.Interaction(gameObject);
    }

    void RaiseHoverEvent() {
        PlayClip(clip);
    }

    void PlayClip(AudioClip clip) {
        audioSource.enabled = true;
        audioSource.clip = clip;
        audioSource.Play();
        UniTask.Action(async () => {
            await UniTask.Delay((int) (clip.length * 1000));
            audioSource.enabled = false;
        });
    }

    public void SetCorrect(GameObjectList.ClickedObjectData clickedObjectData) {
        if (markedCorrect) {
            return;
        }
        MeshRenderer mr = GetComponentInChildren<MeshRenderer>();
        Material[] mats = mr.sharedMaterials;
        Material toSet = clickedObjectData.IsCorrect ? correctOutline : incorrectOutline;
        for (int i = 0; i < mats.Length; i++) {
            mats[i] = toSet;
        }

        mr.sharedMaterials = mats;
        markedCorrect = true;   
        PlayClip(clickedObjectData.IsCorrect ? correct : incorrect);
    }
    
}
