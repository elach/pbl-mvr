﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using UnityEngine;
using static GameObjectList;

public class Minigame1FinishPlate : MonoBehaviour {

    [SerializeField] PrefabManager spawnedObjects;
    [SerializeField] DisplaySet[] sets;

    [SerializeField] List<GameObject> toHideOnCounting;
    bool countingBooks;
    float resetDelay = 3.0f;
    
    [Serializable]
    class DisplaySet {
        [SerializeField] TextMesh label;
        [SerializeField] ButtonSelector toggles;
        ClickedObjectData data;
        string correctPlace;

        public void FillWithData(ClickedObjectData data, IEnumerable<string> names, PrefabManager placementInfo) {
            this.data = data;
            toggles.Init(names);
            label.text = data.ObjectToFind.ToString();
            correctPlace = placementInfo.GetGameObjectsPlace(data.ObjectToFind).placeName;
        }

        public bool IsCorrect() {
            
            toggles.HighlightAnswer(correctPlace);
            return toggles.GetCurrent().Equals(correctPlace);
        }

        public void Show() {
            label.gameObject.SetActive(true);
            toggles.gameObject.SetActive(true); 
        }

        public void Hide() {
            label.gameObject.SetActive(false);
            toggles.gameObject.SetActive(false);
        }
    }
    
    [NaughtyAttributes.Button]
    public void ConfimButton() {
        if (countingBooks) {
            countingBooks = false;
            toHideOnCounting.ForEach(x => x.SetActive(true));
        }
        else {
            OnConfirm();
        }
    }

    void OnConfirm() {
        ResetWithDelay(resetDelay).Forget();
    }

    async UniTaskVoid ResetWithDelay(float delayS) {
        await GamesManager.Instance.EndCurrentGame(delayS);
        gameObject.SetActive(false);
    }
    
    public void ShowPlate() {
        MiniGame1 minigame = GamesManager.Instance.GetActualGame() as MiniGame1;
        if (minigame == null) {
#if UNITY_EDITOR
            Debug.Log("Minigame1 not active");
#endif
            return;
        }
        gameObject.SetActive(true);
        toHideOnCounting.ForEach(x => x.SetActive(false));
        countingBooks = true;

    }
    
    [NaughtyAttributes.Button]
    public void SetupView() {
        List<ClickedObjectData> toQuestion = ObjectsToQuestion();
        IEnumerable<string> allNames = spawnedObjects.PlaceNames();
        foreach (var set in sets) {
            set.Hide();
        }
        FillDisplaysWithQuestions(toQuestion, allNames);
    }

    void FillDisplaysWithQuestions(List<ClickedObjectData> toQuestion, IEnumerable<string> allNames) {
        var enumerator = sets.Zip(toQuestion, (set, data) => {
            set.FillWithData(data, allNames, GamesManager.Instance.prefabManager);
            set.Show();
            return data.ObjectToFind;
        }).GetEnumerator();
        while (enumerator.MoveNext()) { }
        enumerator.Dispose();
    }

    List<ClickedObjectData> ObjectsToQuestion() {
        MiniGame1 minigame = GamesManager.Instance.GetActualGame() as MiniGame1;
        List<ClickedObjectData> allCorrect = minigame.allExerciseObjects.GetListOfObjects()
            .Where(obj => obj.IsCorrect).ToList();

        int[] toSelect = allCorrect.RandomNonRepeatingIndexes(sets.Length);

        List<ClickedObjectData> toQuestion = new List<ClickedObjectData>();
        foreach (int index in toSelect) {
            toQuestion.Add(allCorrect[index]);
        }
        return toQuestion;
    }
}