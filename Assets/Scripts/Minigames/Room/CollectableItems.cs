﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Enum zawierający rodzaje przedmiotów.
/// </summary>
public enum CollectableItems
{
    Keys, Book, Wallet, Salt, Screwdriver, Glasses, Knife, Spoon, Mug,
    Scissors, Hammer, Pliers, Comb, Pear, Carrot, Candle, Toothbrush, Watch, Bottle, Apple

};

/// <summary>
/// Klasa zawierająca narzędzie dotyczące enuma CollectableItems
/// </summary>
class CollectableItemsTools
{
    /// <summary>
    /// FUnkcja pobierająca nazwy dla konkretnej wartości enuma CollectableItems
    /// </summary>
    /// <param name="name">konkretna wartość enuma</param>
    /// <returns>nazwa w string dla podanego enuma</returns>
    public static string getName(CollectableItems name)
    {
        string newName = "";

        switch (name)
        {
            case CollectableItems.Keys:
            newName = "Klucze";
                break;
            case CollectableItems.Book:
                newName = "Książka";
                break;
            case CollectableItems.Wallet:
                newName = "Portfel";
                break;
            case CollectableItems.Salt:
                newName = "Sól";
                break;
            case CollectableItems.Screwdriver:
                newName = "Śrubokręt";
                break;
            case CollectableItems.Glasses:
                newName = "Okulary";
                break;
            case CollectableItems.Knife:
                newName = "Nóż";
                break;
            case CollectableItems.Spoon:
                newName = "Łyżka";
                break;
            case CollectableItems.Mug:
                newName = "Kubek";
                break;
            case CollectableItems.Scissors:
                newName = "Nożyczki";
                break;
            case CollectableItems.Hammer:
                newName = "Młotek";
                break;
            case CollectableItems.Pliers:
                newName = "Kombinerki";
                break;
            case CollectableItems.Comb:
                newName = "Grzebień";
                break;
            case CollectableItems.Pear:
                newName = "Gruszka";
                break;
            case CollectableItems.Carrot:
                newName = "Marchewka";
                break;
            case CollectableItems.Candle:
                newName = "Świeca";
                break;
            case CollectableItems.Toothbrush:
                newName = "Szczoteczka do zębów";
                break;
            case CollectableItems.Watch:
                newName = "Zegarek";
                break;
            case CollectableItems.Bottle:
                newName = "Butelka";
                break;
            case CollectableItems.Apple:
                newName = "Jabłko";
                break;
        }

        
        return newName;
    }
}
