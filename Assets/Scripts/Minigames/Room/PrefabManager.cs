﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Klasa odpowiadająca za zarządzanie prefabami
/// </summary>
public class PrefabManager : MonoBehaviour
{
    [SerializeField]
    private List<FindableItemInfo> PrefabObjects;

    List<PlaceInfo> Places;
    IEnumerable<String> PlacesNames => Places.Select(place => place.placeName);

    /// <summary>
    /// Lista wszystkich utworzonych obiektów na podstawie prefabów
    /// </summary>
    public IReadOnlyList<FindableItemInfo> InstantiatedObjects => instantiatedObjects;
    List<FindableItemInfo> instantiatedObjects = new List<FindableItemInfo>();


    private void Start() {
        Places = new List<PlaceInfo>(FindObjectsOfType<PlaceInfo>());
#if UNITY_EDITOR
        OneToOneFindableItemsToEnums(PrefabObjects);
#endif
    }

    void OneToOneFindableItemsToEnums(IEnumerable<FindableItemInfo> items) {
        Dictionary<CollectableItems, FindableItemInfo> duplicateCheck = new Dictionary<CollectableItems, FindableItemInfo>();
        foreach (FindableItemInfo item in items) {
            try {
                duplicateCheck.Add(item.itemType, item);
            }
            catch (ArgumentException e) {
                Debug.LogError($"Duplicate {item.itemType} found new", item);
                Debug.LogError($"Duplicate {item.itemType} found existing", duplicateCheck[item.itemType]);
            }
        }

        IEnumerable<CollectableItems> missingCheck = Enum.GetValues(typeof(CollectableItems)).Cast<CollectableItems>();
        foreach (CollectableItems i in missingCheck) {
            if (items.FirstOrDefault(item => item.itemType == i) == null) {
                Debug.LogError($"Missing {i}");
            }
        }
    }
    

    public FindableItemInfo GetPrefab(CollectableItems item) {
        return PrefabObjects.First(i => i.itemType == item);
    }

    /// <summary>
    /// Zwraca nazwę miejsca w którym znajduje się przedmiot
    /// </summary>
    /// <param name="gameObject">Przedmiot dla którego szukamy miejsca</param>
    /// <returns>Nazwa miejsca w którym znajduje się przedmiot</returns>
    public PlaceInfo GetGameObjectsPlace(CollectableItems gameObject)
    {
        foreach (FindableItemInfo item in instantiatedObjects)
        {
            if (item && item.itemType == gameObject)
            {
                return item.place;
            }
        }
        return null;
    }

    public IEnumerable<string> PlaceNames() {
        return instantiatedObjects
            .GroupBy(obj => obj.place.placeName)
            .Select(group => group.Key);
    }

    /// <summary>
    /// Usuwa wszystkie utworzone przedmioty
    /// </summary>
    public void ClearObjects() {
        foreach (FindableItemInfo t in instantiatedObjects) {
            GameObject.Destroy(t.gameObject);
        }
        instantiatedObjects.Clear();
    }
    /// <summary>
    /// Przypisuje miejsca i tworzy przedmioty w dostępnych miejscach w pokoju
    /// </summary>
    /// <param name="objectsToFind">lista obiektów które należy znaleść (więc muszą być umieszczone)</param>
    /// <param name="fakeObjectsCount">ile jest losowanych obiektów nieprawidłowych (dystrakcyjnych)</param>
    /// <param name="active">czy utworzone obiekty mają być aktywne</param>
    /// <returns>List stworzonych niepoprawnych obiektów</returns>
    public List<FindableItemInfo> FindPlaces(List<FindableItemInfo> objectsToFind, int fakeObjectsCount, bool active = true) {
        ClearObjects();
        
        List<int> ThingsToPut = new List<int>(objectsToFind.Count + fakeObjectsCount);
        List<FindableItemInfo> objects = new List<FindableItemInfo>(objectsToFind.Count+ fakeObjectsCount);

        for (int i = 0; i < objectsToFind.Count; i++){
            objects.Add(objectsToFind[i]);
        }
        
        var objectsBad = SpawnBadObjects(fakeObjectsCount, objects);
        SpawnGoodObjects(objects, ThingsToPut, active);
        return objectsBad;
    }

    void SpawnGoodObjects( List<FindableItemInfo> objects,  List<int> ThingsToPut, bool active ) {
        for (int i = 0; i < objects.Count; i++) {
            bool found = false;
            int number;
            do {
                found = false;
                number = UnityEngine.Random.Range(0, Places.Count);
                if (ThingsToPut != null) {
                    for (int j = 0; j < ThingsToPut.Count; j++) {
                        if (number == ThingsToPut[j]) {
                            j = ThingsToPut.Count;
                            found = true;
                        }
                    }
                }
            } while (found != false);

            GameObject inst = PlaceObject(objects[i].gameObject, Places[number].transform);
            StaticBatchingUtility.Combine(inst);
            var oi = inst.GetComponent<FindableItemInfo>();
            instantiatedObjects.Add(oi);
            ThingsToPut.Add(number);
            inst.SetActive(active);
            if (oi != null) {
                oi.place = Places[number].GetComponent<PlaceInfo>();
            }
        }

    }

    List<FindableItemInfo> SpawnBadObjects(int fakeObjectsCount, List<FindableItemInfo> objects) {
        List<FindableItemInfo> objectsBad = new List<FindableItemInfo>(fakeObjectsCount);
        for (int i = 0; i < fakeObjectsCount; i++) {
            bool found = false;
            int number;
            do {
                number = UnityEngine.Random.Range(0, PrefabObjects.Count);

                if (objects != null) {
                    for (int j = 0; j < objects.Count; j++) {
                        if (objects[j].itemType == PrefabObjects[number].itemType) {
                            j = objects.Count;
                            found = true;
                        }
                    }
                }
            } while (found != false);

            objects.Add(PrefabObjects[number]);
            objectsBad.Add(PrefabObjects[number]);
        }

        return objectsBad;
    }


    GameObject PlaceObject(GameObject prefab, Transform parent) {
        Transform toPlace = Instantiate(prefab, parent).transform;
        Collider[] cols = toPlace.GetComponentsInChildren<Collider>(false);
        foreach (Collider col in cols) {
            col.enabled = false;
        }
        RaycastHit hit; 
        if(Physics.Raycast(toPlace.position, Vector3.down, out hit)) {
            toPlace.position = hit.point;
        }
        foreach (Collider col in cols) {
            col.enabled = true;
        }

        return toPlace.gameObject;
    }
   
}
