﻿using Cysharp.Threading.Tasks;
using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.Input;
using UnityEngine;

namespace Minigames.Room {
    /// <summary>
    /// Klasa odpowiadająca za połączenie wybranych ustawień z ekranu badacza z konfiguracjami w badaniu związanego ze znalezieniem przedmiotów i ponownym wskazniau ich położenia
    /// </summary>
    public class MiniGame1Setup :MinigameSetup  {
        
        [SerializeField] MiniGame1 game;

        public override void GameStart(SettingsMinigame objectsToFind) {
            GamesManager.Instance.StartGame(game, objectsToFind);
        }

        protected override int GetMinigameId() {
            return 1;
        }

        #if UNITY_EDITOR
        [NaughtyAttributes.Button()]
        void QuickStartEditorButton() {
            base.QuickStart();
        }
        #endif

    }
}