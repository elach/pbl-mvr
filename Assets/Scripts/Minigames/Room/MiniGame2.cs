﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using Minigames.Room;
using UnityEngine;
using static GameObjectList;

/// <summary>
///     Zadanie 2 - polegające na zapamiętywaniu i odnajdywaniu konkretnych przedmiotów
/// </summary>
public class MiniGame2 : MiniGameBase {
    /// <summary>
    ///     Enum zawierający tryby dla zadania
    /// </summary>
    public enum MiniGame2Mode {
        ShowingObjects,
        HearingObjects,
        ShowingAndHearingObjects
    }

    /// <summary>
    ///     Instrukcje głosowe dla trybu gry: ShowingObjects
    /// </summary>
    public AudioInstruction InstructionForExerciseShow;

    /// <summary>
    ///     Instrukcje głosowe dla trybu gry: HearingObjects
    /// </summary>
    public AudioInstruction InstructionForExerciseHear;

    /// <summary>
    ///     Instrukcje głosowe dla trybu gry: ShowingAndHearingObjects
    /// </summary>
    public AudioInstruction InstructionForExerciseHearAndShow;

    [SerializeField] List<Transform> PlaceToShowItems = new List<Transform>();

    float lengthOfShowing = 10;
    float lengthOfShowingWithInstruction;
    MiniGame2Mode modeShowingObjects = MiniGame2Mode.ShowingObjects;

    List<GameObject> objectsToFindInstantiated = new List<GameObject>();
    readonly float postEndCheckDelayS = 0.5f;

    /// <summary>
    ///     Metoda uruchamiana na koniec zadania w celu zapisu informacji i posprzątania po zadaniu
    /// </summary>
    public override void EndGame() {
        base.EndGame();
        PrefabManager.ClearObjects();
    }


    /// <summary>
    ///     Metoda która rozpoczyna grę
    /// </summary>
    /// <param name="settings"> ustawienia gry</param>
    public override void StartGame(SettingsMinigame settings) {
        base.StartGame(settings);
        SetGameMode(settings.mode, settings.displayTime);
        PlayStartInstruction(true);
    }

    /// <summary>
    ///     Metoda rozpatruje wybranie przedmiotu
    /// </summary>
    /// <param name="findableItemInfo"> wybrany przedmiot</param>
    public override ClickedObjectData? ItemSelected(FindableItemInfo findableItemInfo) {
        var clicked = base.ItemSelected(findableItemInfo);
        if (clicked != null) {
            findableItemInfo.SetCorrect(clicked.Value);
            AutoEndCheck();
        }

        return clicked;
    }

    /// <summary>
    ///     Metoda aktywująca ukryte przedmioty podczas rozpoczęcia właściwej części zadania
    /// </summary>
    void BeginExercise() {
        var inst = PrefabManager.InstantiatedObjects;
        for (var i = 0; i < inst.Count; i++) inst[i].gameObject.SetActive(true);
    }

    /// <summary>
    ///     Metoda ustawia tryb zadania
    /// </summary>
    /// <param name="showObjectGameMode"> ustawiany tryb zadania </param>
    /// <param name="lengthOfShowing"> czas trwania etapu zapoznania się z przedmiotami</param>
    void SetGameMode(MiniGame2Mode showObjectGameMode, float lengthOfShowing) {
        modeShowingObjects = showObjectGameMode;
        this.lengthOfShowing = lengthOfShowing;
    }

    /// <summary>
    ///     Metoda uruchamia instrukcje startową (głosową) dla zadania
    /// </summary>
    void PlayStartInstruction(bool autoBegin = false) {
        lengthOfShowingWithInstruction = Mathf.Max(0, lengthOfShowing);
        switch (modeShowingObjects) {
            case MiniGame2Mode.ShowingObjects:
                audioInstructions.setAudioInstruction(InstructionForExerciseShow);
                lengthOfShowingWithInstruction += audioInstructions.StartInstruction();
                ShowItems();
                break;

            case MiniGame2Mode.HearingObjects:
                audioInstructions.setAudioInstruction(InstructionForExerciseHear);
                lengthOfShowingWithInstruction += audioInstructions.StartInstruction(true);
                break;

            case MiniGame2Mode.ShowingAndHearingObjects:
                audioInstructions.setAudioInstruction(InstructionForExerciseHearAndShow);
                lengthOfShowingWithInstruction += audioInstructions.StartInstruction(true);
                ShowItems();
                break;
        }

        if (autoBegin) BeginWithDelay().Forget();
    }

    async UniTaskVoid BeginWithDelay() {
        var delayLength = (int) (lengthOfShowingWithInstruction * 1000);
        await UniTask.Delay(delayLength);
        BeginExercise();
    }


    void AutoEndCheck() {
        var objects = allExerciseObjects.GetListOfObjects();
        var shouldEnd = objects
            .Where(x => x.IsCorrect)
            .All(x => x.AnswersCount > 0);
        if (shouldEnd) GamesManager.Instance.EndCurrentGame(postEndCheckDelayS).Forget();
    }

    void DeleteShowingItems() {
        if (objectsToFindInstantiated != null) {
            for (var i = 0; i < objectsToFindInstantiated.Count; i++)
                if (objectsToFindInstantiated != null)
                    Destroy(objectsToFindInstantiated[i]);
            objectsToFindInstantiated = null;
        }
    }

    void ShowItems() {
        DeleteShowingItems();
        objectsToFindInstantiated = new List<GameObject>();
        for (var i = 0; i < allExerciseObjects.GetListOfObjects().Count; i++) {
            GameObject objectToInstantiate = PrefabManager.GetPrefab(allExerciseObjects.GetObjectToFind(i)).gameObject;
            GameObject inst = Instantiate(objectToInstantiate, PlaceToShowItems[i]);

            FindableItemInfo info = inst.GetComponent<FindableItemInfo>();
            info.cannotTouch = true;
            objectsToFindInstantiated.Add(inst);
        }
        DeleteShowingItemsWithDelay().Forget();
    }

    async UniTaskVoid DeleteShowingItemsWithDelay() {
        await UniTask.Delay((int)(lengthOfShowingWithInstruction * 1000));
        DeleteShowingItems();
    }
}