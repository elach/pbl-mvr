﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.MixedReality.Toolkit.UI;
using Microsoft.MixedReality.Toolkit.Utilities;
using UnityEngine;

[RequireComponent(typeof(GridObjectCollection))]
[RequireComponent(typeof(InteractableToggleCollection))]
public class ButtonSelector : MonoBehaviour {
    [SerializeField] GameObject radialPrefab;

    List<RadialData> spawnedButtons = new List<RadialData>();

    GridObjectCollection grid;
    Color correctColor = Color.green;
    InteractableToggleCollection interactables;

    class RadialData {
        public readonly Interactable interactable;
        public readonly TextMesh textMesh;

        public RadialData(GameObject init, string name) {
            interactable = init.GetComponent<Interactable>();
            interactable.IsToggled = false;
            textMesh = init.GetComponentInChildren<TextMesh>();
            textMesh.text = name;
        }

        public void Destroy() {
            interactable.transform.parent = null;
            Object.Destroy(interactable.gameObject);
        }
    }

    public void Init(IEnumerable<string> values) {
        interactables = GetComponent<InteractableToggleCollection>();
        grid = GetComponent<GridObjectCollection>();
        ReinitButtons(values);
        interactables.ToggleList = spawnedButtons
            .Select(el => el.interactable).ToArray();
        grid.UpdateCollection();
    }

    public string GetCurrent() {
        if (interactables.CurrentIndex < 0) {
            return string.Empty;
        }
        return spawnedButtons[interactables.CurrentIndex].textMesh.text;
    }

    public void HighlightAnswer(string answerName) {
        spawnedButtons.First(el => el.textMesh.text == answerName)
            .textMesh.color = correctColor;
    }

    void ReinitButtons(IEnumerable<string> values) {
        foreach (RadialData radial in spawnedButtons) {
            radial.Destroy();
        }
        spawnedButtons.Clear();
        foreach (string value in values) {
            GameObject newButton = Instantiate(radialPrefab, transform);
            spawnedButtons.Add(new RadialData(newButton, value));
        }
    }

}