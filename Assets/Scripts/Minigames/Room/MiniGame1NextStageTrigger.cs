﻿using Microsoft.MixedReality.Toolkit.Input;
using UnityEngine;

public class MiniGame1NextStageTrigger : MonoBehaviour {
    [SerializeField] MiniGame1 minigame;

    
    void OnTriggerEnter(Collider other) {
        if (other.GetComponent<GazeProvider>() != null) {
         LaunchNextStep();   
        }
    }
   
   [NaughtyAttributes.Button] 
    void LaunchNextStep(){
        minigame.NextGameStep();
    }
}
