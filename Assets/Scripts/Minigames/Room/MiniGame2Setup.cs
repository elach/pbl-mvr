﻿using UnityEngine;

namespace Minigames.Room {
    /// <summary>
    /// Klasa odpowiadająca za przekazanie wybranych ustawień z ekranu badacza do odpowiedniego wybranego trybu badania.
    /// </summary>
    public class MiniGame2Setup : MinigameSetup
    {
        [SerializeField] MiniGame2 game;
        protected override int GetMinigameId() => 2;
        
        public override void GameStart(SettingsMinigame settingsLevel) {
            GamesManager.Instance.StartGame(game, settingsLevel);
        }

#if UNITY_EDITOR
        [NaughtyAttributes.Button()]
        void QuickStartEditorButton() {
            base.QuickStart();
        }
#endif
    }
}
