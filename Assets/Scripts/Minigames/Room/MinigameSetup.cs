﻿using System;
using Cysharp.Threading.Tasks;
using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.Input;
using UnityEngine;

namespace Minigames.Room {
    [Serializable]
    public abstract class MinigameSetup : MonoBehaviour, IMixedRealityInputActionHandler {
        [SerializeField] SettingsMinigame defaultDiff;
        [SerializeField] MixedRealityInputAction quickStartAction;
        [SerializeField] SheetPull dataFromGoogleSheet;

        public abstract void GameStart(SettingsMinigame difficultyLevel);
        protected abstract int GetMinigameId();
        
        void OnEnable() {
            CoreServices.InputSystem?.RegisterHandler<IMixedRealityInputActionHandler>(this);
        }

        void OnDisable() {
            CoreServices.InputSystem?.UnregisterHandler<IMixedRealityInputActionHandler>(this);
        }


        protected void GameStartDefault() {
            GameStart(defaultDiff);
        }

       
        protected async UniTaskVoid QuickStart() {
            SettingsMinigame diff = await dataFromGoogleSheet.GetDifficulty(GetMinigameId());
            if (diff == null) {
                GameStartDefault();
            }
            else {
                GameStart(diff);
            }
        }
        
        public void OnActionStarted(BaseInputEventData eventData) {
            if (eventData.MixedRealityInputAction.Id == quickStartAction.Id) {
                QuickStart().Forget();
            }
        }

        public void OnActionEnded(BaseInputEventData eventData) {

        }
        
    }
}