﻿using System.Collections;
using System.Collections.Generic;
using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.Utilities;
using UnityEngine;
using static PrefabManager;

/// <summary>
/// Skrypt umożliwiający odtworzenie instrukcji
/// </summary>
public class AudioInstructions : MonoBehaviour
{
    

    /// <summary>
    /// Referencja do menadźera prefabów
    /// </summary>
    public PrefabManager PrefabManager;

    /// <summary>
    /// Lista plików dźwiękowych dla obiektów które występują w zadaniu
    /// </summary>
    public List<AudioClip> objectsCount = new List<AudioClip>();

    [SerializeField] AudioSource[] audioSources;
    AudioSource audioSource => audioSources[0];

    private AudioInstruction audioInstruction;

    private List<CollectableItems> itemsToFind = new List<CollectableItems>();

    private Queue<AudioClip> queue = new Queue<AudioClip>();

    /// <summary>
    /// przerwa między wymienianymi obiektami
    /// </summary>
    public int delayBetweenObjects = 1;

    private float timer = 0;
    void Update()
    {
        if (!audioSource.isPlaying)
        {
            timer += Time.deltaTime;
        }
        
        if (!audioSource.isPlaying && queue.Count > 0 && timer >= delayBetweenObjects)
        {
            timer = 0;
            SetClipAndPlay(queue.Dequeue());
        }

   
    }

    void SetClipAndPlay(AudioClip clip) {
        foreach (AudioSource s in audioSources) {
            s.clip = clip;
            s.Play();
        }    
    }
    

    /// <summary>
    /// Ustawienie elementów do odnalezienia
    /// </summary>
    /// <param name="itemsToFind">lista elementów do odnalezienia</param>
    public void setItemsToFind(List<CollectableItems> itemsToFind)
    {
        this.itemsToFind = itemsToFind;
    }

    /// <summary>
    /// ustawienie instrukcji dźwiękowej
    /// </summary>
    /// <param name="audioInstruction">instrukcja dźwiękowa</param>
    public void setAudioInstruction(AudioInstruction audioInstruction)
    {
        this.audioInstruction = audioInstruction;
    }

    /// <summary>
    /// metoda zatrzymująca odtwarzanie instrukcji
    /// </summary>
    public void ToStop()
    {
        foreach (AudioSource s in audioSources) {
            s.Stop();
        }    
        queue.Clear();
    }

    /// <summary>
    /// Metoda rozpoczynająca odtwarzanie instrukji
    /// </summary>
    /// <param name="withItems">czy odtwarzać z przedmiotami</param>
    /// <returns>czas trwania instrukcji</returns>
    public float StartInstruction(bool withItems = false)
    {
        ToStop();
        queue.Enqueue(audioInstruction.begining);
        timer = delayBetweenObjects;

        if (audioInstruction.end)
        {
            if (objectsCount.Count < itemsToFind.Count)
            {
                queue.Enqueue(objectsCount[objectsCount.Count - 1]);
            }
            else
            {
                queue.Enqueue(objectsCount[itemsToFind.Count - 1]);
            }

            queue.Enqueue(audioInstruction.end);
        }

        if (withItems == true)
        {
            StartHearingItems();
        }

        return calculateTime();
    }

    /// <summary>
    /// obliczenie czasu trwania instrukcji
    /// </summary>
    /// <returns>czas trwania instrukcji</returns>
    public float calculateTime()
    {
        float time = delayBetweenObjects * itemsToFind.Count;
        foreach (AudioClip clip in queue) {
            time += clip.length;
        }
        return time;
    }

    /// <summary>
    /// Odtwarzanie przedmiotów
    /// </summary>
    /// <returns>czas trwania instrukji</returns>
    public float StartItems()
    {
        ToStop();
        return StartHearingItems();
    }

    private float StartHearingItems()
    {
        timer = delayBetweenObjects;
        float delay = 0;
        for (int i = 0; i < itemsToFind.Count; i++)
        {
            FindableItemInfo item = PrefabManager.GetPrefab(itemsToFind[i]);
            queue.Enqueue(item.clip);
            delay += item.clip.length + delayBetweenObjects;
        }
        return delay;
    }
}
