﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using GoogleSheetsToUnity;
using UnityEngine;

namespace Minigames.Room {
    
    [CreateAssetMenu(fileName = "Settings/Minigame")]
    public class SettingsMinigame : ScriptableObject  {
        
        public int toFindCount => mToFindCount;
        [SerializeField] int mToFindCount;
        const string toFindKey = "toFind";
        public int redHerringCount => mRedHerringCount;
        [SerializeField] int mRedHerringCount;
        const string redHerringKey = "redHerring";

        public float displayTime => mDisplayTime;
        [Header("Minigame2 specific")]
        [SerializeField] float mDisplayTime = 5.0f;
        const string displayTimeKey = "displayTime";
        
        public MiniGame2.MiniGame2Mode mode => mMode;
        [SerializeField] MiniGame2.MiniGame2Mode mMode;
        const string modeKey = "mode";
        
        
        public void SetupFromSheet(GstuSpreadSheet sheet, int minigameId) {
            string rowAdress = minigameId.ToString();
            try {
                mToFindCount = int.Parse(sheet[rowAdress, toFindKey].value);
                mRedHerringCount = int.Parse(sheet[rowAdress, redHerringKey].value);
                mDisplayTime = float.Parse(sheet[rowAdress, displayTimeKey].value);
                mMode = (MiniGame2.MiniGame2Mode)Enum.Parse(
                    typeof(MiniGame2.MiniGame2Mode), 
                    sheet[rowAdress, modeKey].value);
            }
            catch (FormatException e) {
                mToFindCount = 1;
                mRedHerringCount = 1;
                mDisplayTime = 1;
                mMode = MiniGame2.MiniGame2Mode.ShowingAndHearingObjects;
            }


        }
    }

    
}