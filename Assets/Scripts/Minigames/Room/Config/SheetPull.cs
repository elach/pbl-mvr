﻿using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using GoogleSheetsToUnity;
using Minigames.Room;
using Newtonsoft.Json;
using UnityEngine;

public class SheetPull : MonoBehaviour {
    
    [SerializeField] string sheetId = "18nqcoSiSv2ngQTmWSlo_jhONp3UjuHk34fWHK6k2V38";
    [SerializeField] string sheetName = "Arkusz1";
    

    public async UniTask<SettingsMinigame> GetDifficulty(int minigameId){
        GstuSpreadSheet sheet = await SpreadsheetManager.ReadPublicSpreadsheet(new GSTU_Search(sheetId, sheetName));
        SettingsMinigame toReturn = ScriptableObject.CreateInstance<SettingsMinigame>() ;
        toReturn.SetupFromSheet(sheet, minigameId);
        return toReturn;
    }
    
}
