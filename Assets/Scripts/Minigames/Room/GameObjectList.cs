﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Klasa przedstawiająca listę obiektów (przedmiotów) występujących w zadaniu
/// </summary>
public class GameObjectList
{
    /// <summary>
    /// Informacja o występującym przedmiocie
    /// </summary>
    [System.Serializable]
    public struct ClickedObjectData
    {
        /// <summary>
        /// Typ przedmiotu
        /// </summary>
        public CollectableItems ObjectToFind;
        /// <summary>
        /// Ile razy został wybrany
        /// </summary>
        public int AnswersCount;
        /// <summary>
        /// Czy jest poszukiwanym obiektem
        /// </summary>
        public bool IsCorrect;
        /// <summary>
        /// Czy został zapamiętany przed rozpoczęciem poszukiwań
        /// </summary>
        public bool answeredAtTheTestBeforExercise;

        /// <summary>
        /// Konstruktor dla struktury
        /// </summary>
        /// <param name="ObjectToFind"> typ obiektu </param>
        /// <param name="IsCorrect"> czy jest to poszukiwany obiekt </param>
        public ClickedObjectData(CollectableItems ObjectToFind, bool IsCorrect)
        {
            this.ObjectToFind = ObjectToFind;
            this.IsCorrect = IsCorrect;
            this.AnswersCount = 0;
            this.answeredAtTheTestBeforExercise = false;
        }
    }

    private List<ClickedObjectData> ListOfAllObjects = new List<ClickedObjectData>();

    /// <summary>
    /// Pobieranie listy przedmiotów
    /// </summary>
    /// <returns> lista przedmiotów </returns>
    public IReadOnlyList<ClickedObjectData> GetListOfObjects() => ListOfAllObjects;

    /// <summary>
    /// Pobierz jeden przedmiot z listy
    /// </summary>
    /// <param name="index"> indeks przedmiotu</param>
    /// <returns>przedmiot</returns>
    public CollectableItems GetObjectToFind(int index)
    {
        return ListOfAllObjects[index].ObjectToFind;
    }

    /// <summary>
    /// Wyczyść listę przedmiotów 
    /// </summary>
    public void ClearList()
    {
        ListOfAllObjects = new List<ClickedObjectData>();
    }

    /// <summary>
    /// Metoda sprawdza czy przekazany typ przedmiotu znajduje się w liście
    /// </summary>
    /// <param name="collectableItems"> typ sprawdzanego obiektu </param>
    /// <returns>true jeśli lista zwiera obiekt, w przeciwnym razie false</returns>
    public bool ContainsObject(CollectableItems collectableItems)
    {
        foreach (ClickedObjectData objectFind in ListOfAllObjects)
        {
            if (objectFind.ObjectToFind == collectableItems)
                return true;
        }

        return false;
    }

    /// <summary>
    /// Metoda dodaje przedmiot do listy
    /// </summary>
    /// <param name="obj">dodawany przedmioty</param>
    public void Add(ClickedObjectData obj)
    {
        ListOfAllObjects.Add(obj);
    }

    /// <summary>
    /// Metoda zwraca idneks poszukiwanego przedmiotu
    /// </summary>
    /// <param name="collectableItems">poszukiwany przedmiot</param>
    /// <returns>indeks przedmiotu</returns>
    public int indexOfObject(CollectableItems collectableItems)
    {
        for (int i = 0; i < ListOfAllObjects.Count; i++)
        {
            if (ListOfAllObjects[i].ObjectToFind == collectableItems)
                return i;
        }

        return -1;
    }

    /// <summary>
    /// Metoda zwraca dane dla poszukiwanego przedmiotu
    /// </summary>
    /// <param name="collectableItems">poszukiwany przedmiot</param>
    /// <returns>dane przedmiotu</returns>
    public ClickedObjectData? GetClickedObjectData(CollectableItems collectableItems)
    {
        for (int i = 0; i < ListOfAllObjects.Count; i++)
        {
            if (ListOfAllObjects[i].ObjectToFind == collectableItems)
                return ListOfAllObjects[i];
        }

        return null;
    }

    /// <summary>
    /// Metoda zwraca ilość zazanczeń obiektu
    /// </summary>
    /// <param name="index">obiekt któremu zwiększamy ilość zaznaczeń</param>
    /// <returns>obiekt któremu została zwiększona ilość zaznaczeń</returns>
    public ClickedObjectData AddAnswerCount(int index)
    {
        ClickedObjectData click = ListOfAllObjects[index];
        click.AnswersCount++;
        ListOfAllObjects[index] = click;
        return click;
    }

    /// <summary>
    /// Metoda ustawiająca zmienną answeredAtTheTestBeforExercise na podaną wartość 
    /// </summary>
    /// <param name="collectableItems"> przedmiot dla którego ustawiamy wartość </param>
    /// <param name="state"> wartość dla ustawianej zmiennej </param>
    public void SetCheckedAnsweredAtTheTestBeforExercise(CollectableItems collectableItems, bool state)
    {
        var index = indexOfObject(collectableItems);
        ClickedObjectData click = ListOfAllObjects[index];
        click.answeredAtTheTestBeforExercise = state;
        ListOfAllObjects[index] = click;
    }

}
