﻿using UnityEngine;

/// <summary>
/// struktura przetrzymująca poczatek pliku dźwiękowego i jego koniec (instrukcja dźwiękowa)
/// </summary>
[System.Serializable]
public struct AudioInstruction
{
    /// <summary>
    /// początek instrukcji
    /// </summary>
    public AudioClip begining;

    /// <summary>
    /// koniec instrukcji dźwiękowej
    /// </summary>
    public AudioClip end;
}