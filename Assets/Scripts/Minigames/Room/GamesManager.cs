﻿using System;
using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.UI;
using Minigames.Room;
using UniRx;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Klasa będąca menadżerem dla konkretnych zdań
/// </summary>
public class GamesManager : Singleton<GamesManager> {

    public UnityEvent OnAnyGameStart;
    public UnityEvent OnAnyGameEnd;
    
    public event Action<MiniGameBase> onGameStart;

    ReactiveProperty<MiniGameBase> actualGameProp = new ReactiveProperty<MiniGameBase>(null);

    MiniGameBase actualGame {
        set {
            actualGameProp.Value = value;
        }
        get {
            return actualGameProp.Value;
        }
    }
    
    public PrefabManager prefabManager => mPrefabManager;
    [NaughtyAttributes.Label("Prefab Manager")]
    [SerializeField] PrefabManager mPrefabManager;

    void Awake() {
        SetupEvents();
    }

    void SetupEvents() {
        actualGameProp.Subscribe(x => {
            if (x == null) {
                OnAnyGameEnd.Invoke();
            }
            else {
                onGameStart?.Invoke(x);
                OnAnyGameStart.Invoke();    
            }
        }).AddTo(this);
    }

    public async UniTask EndCurrentGame(float delayS = 0) {
        await UniTask.Delay((int)(delayS * 1000));
        actualGame?.EndGame();
        actualGame = null;
    }

    public bool IsGameInProgress() => actualGame != null;

    public void StartGame(MiniGameBase game, SettingsMinigame settings) {
        if (actualGame != null) {
            return;
        }
        game.StartGame(settings);
        actualGame = game;
    }
    
    public MiniGameBase GetActualGame() => actualGame;

    public void Interaction(GameObject collider) {
        if (actualGame != null) {
            actualGame.ItemSelected(collider.GetComponent<FindableItemInfo>());
        }
    }
}