﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Klasa zawierająca informacje o nazwie miejsca.
/// </summary>
public class PlaceInfo : MonoBehaviour {
    public string placeName => _placeName;
    [SerializeField] string _placeName;

    [NaughtyAttributes.Button("Name From Game Object")]
    void GetName() {
        #if UNITY_EDITOR
        UnityEditor.Undo.RecordObject(this, "Set name");
        #endif
        _placeName = gameObject.name;
    }

}
