﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Skrypt przygotowujący zadanie dystrakcyjne
/// </summary>
public class BooksToCount : MonoBehaviour
{
    /// <summary>
    /// Lista obiektów (książek)
    /// </summary>
    public List<GameObject> Books = new List<GameObject>();

    /// <summary>
    /// Numery książek które zostaną wyświetone
    /// </summary>
    public List<int> booksNubers = new List<int>();

    /// <summary>
    /// Metoda ukrywająca wszystkie książki
    /// </summary>
    public void hideAllBooks()
    {
        for (int i = 0; i < Books.Count; i++)
        {
            Books[i].SetActive(false);
        }
    }

    /// <summary>
    /// Metoda ustawiająca widoczność
    /// </summary>
    /// <param name="books">lista książek</param>
    public void setBooksPosition(List<int> books)
    {
        foreach (int book in books)
        {
            Books[book].SetActive(true);
        }
    }

    /// <summary>
    /// Metoda przygotowująca zadanie dystrakcyjne - losująca ilość książek i ich pozycje
    /// </summary>
    public void setBooksPosition()
    {
        System.Random random = new System.Random();
        int numberOfBooks = Math.Abs(random.Next()* 100);
        numberOfBooks = 4 + (numberOfBooks % 7);

        List<GameObject> NewListOfBooks = new List<GameObject>();
        int book = 0;
        List<int> numbersToCheck = new List<int>();
        for (int i = 0; i < numberOfBooks; i++)
        {
            book = Math.Abs(random.Next() * 100) % 10;
            for (int j = 0; j < numbersToCheck.Count; j++)
            {
                if (book == numbersToCheck[j])
                {
                    if (book == 10)
                    {
                        book = 0;
                        j = 0;
                    }
                    else
                    {
                        book += 1;
                        j = 0;
                    }

                }
            }

            
            if (Books.Count > book)
            {
                numbersToCheck.Add(book);
                Books[book].SetActive(true);
                NewListOfBooks.Add(Books[book]);
            }
        }
        booksNubers = numbersToCheck;
    }

    // Start is called before the first frame update
    void Start()
    {
        hideAllBooks();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
