﻿using System;
using System.Collections.Generic;
using Minigames.Room;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;
using static GameObjectList;


/// <summary>
/// Zadanie 1 polegające na zapamiętywaniu miejsc w których znajdują się konkretne przedmioty
/// </summary>
public class MiniGame1 : MiniGameBase{
   

    /// <summary>
    /// Instrukcje głosowe dla początku zadania
    /// </summary>
    public AudioInstruction InstructionForExercise;

    /// <summary>
    /// Instrukcje głosowe dla etapu dystrakcyjnego
    /// </summary>
    public AudioInstruction GoToBedromInstruction;


    /// <summary>
    /// Skrypt odpowiedzialny za dystrakcyjne zadanie. Liczenie książek
    /// </summary>
    public BooksToCount BooksToCount;

    [SerializeField] Minigame1FinishPlate finishPlate;


    /// <summary>
    /// Metoda która rozpoczyna grę
    /// </summary>
    /// <param name="settings"> ustawienia gry</param>
    public override void StartGame(SettingsMinigame settings) {
        base.StartGame(settings);
        PlayStartInstruction();
    }

    /// <summary>
    /// Metoda uruchamiana na koniec zadania w celu zapisu informacji i posprzątania po zadaniu
    /// </summary>
    public override void EndGame() {
        base.EndGame();
        DeleteRoomItems();
        BooksToCount.hideAllBooks();
    }

    /// <summary>
    /// Metoda uruchamia kolejny etap zadania
    /// </summary>
    public void NextGameStep() {
        if (GamesManager.Instance.GetActualGame() != this) {
            return;
        }
        finishPlate.SetupView();
        DeleteRoomItems();
        BooksToCount.setBooksPosition();
        audioInstructions.setAudioInstruction(GoToBedromInstruction);
        audioInstructions.StartInstruction(false);
        finishPlate.ShowPlate();
    }


    /// <summary>
    /// Metoda uruchamia instrukcje startową (głosową) dla zadania
    /// </summary>
    void PlayStartInstruction() {
        audioInstructions.setAudioInstruction(InstructionForExercise);
        audioInstructions.StartInstruction(true);
    }

    /// <summary>
    /// Metoda usuwa wszystkie przedmioty na scenie
    /// </summary>
    void DeleteRoomItems() {
        PrefabManager.ClearObjects();
    }
}