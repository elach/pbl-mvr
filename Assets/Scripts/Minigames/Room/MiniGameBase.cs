﻿using System;
using System.Collections.Generic;
using Microsoft.MixedReality.Toolkit;
using Minigames.Room;
using UnityEngine;

public abstract class MiniGameBase : MonoBehaviour {
     protected PrefabManager PrefabManager => GamesManager.Instance.prefabManager;

     /// <summary>
    /// Skrypt odpowiadający za odtworzenie instrukcji głosowych
    /// </summary>
    [SerializeField] protected AudioInstructions audioInstructions;

    /// <summary>
    /// Pozycja startowa gracza
    /// </summary>
    [SerializeField] Transform playerStartPosition;


    /// <summary>
    /// Klasa zawierająca listę obiektów dostępnych w zadaniu
    /// </summary>
    public GameObjectList allExerciseObjects = new GameObjectList();

    
    /// <summary>
    /// Metoda która rozpoczyna grę
    /// </summary>
    /// <param name="settings"> ustawienia gry</param>
    public virtual void StartGame(SettingsMinigame settings) {
        SetPlayerStartPosition();
        SetupObjects(settings);
    }

    /// <summary>
    /// Metoda uruchamiana na koniec zadania w celu zapisu informacji i posprzątania po zadaniu
    /// </summary>
    public virtual void EndGame() {
        audioInstructions.ToStop();
    }

    /// <summary>
    /// Metoda rozpatruje wybranie przedmiotu
    /// </summary>
    /// <param name="findableItemInfo"> wybrany przedmiot</param>
    public virtual GameObjectList.ClickedObjectData? ItemSelected(FindableItemInfo findableItemInfo) {
        if (findableItemInfo != null && !findableItemInfo.cannotTouch) {
            int index = allExerciseObjects.indexOfObject(findableItemInfo.itemType);
            if (index >= 0) {
                var clicked = allExerciseObjects.AddAnswerCount(index);
                return clicked;
            }
        }

        return null;
    }

    /// <summary>
    /// Metoda przenosi obiekt (gracza) na miejsce startowe
    /// </summary>
    void SetPlayerStartPosition() {
        MixedRealityPlayspace.Position = playerStartPosition.position;
        MixedRealityPlayspace.Rotation = playerStartPosition.rotation;
    }
    
    void SetupObjects(SettingsMinigame settings) {
        var allObjectsCount = Enum.GetValues(typeof(CollectableItems)).Length;
        allExerciseObjects.ClearList();
        List<CollectableItems> itemsAudio = new List<CollectableItems>();
        List<FindableItemInfo> listToFind = new List<FindableItemInfo>();
        for (int i = 0; i < settings.toFindCount; i++) {
            CollectableItems itemToFind;
            do {
                itemToFind = (CollectableItems) (UnityEngine.Random.Range(0, allObjectsCount));
            } while (allExerciseObjects.ContainsObject(itemToFind));

            allExerciseObjects.Add(new GameObjectList.ClickedObjectData(itemToFind, true));
            listToFind.Add(PrefabManager.GetPrefab(itemToFind));
            itemsAudio.Add(itemToFind);
        }

        var hiddenObjects = PrefabManager.FindPlaces(listToFind, settings.redHerringCount);

        for (int i = 0; i < hiddenObjects.Count; i++) {
            allExerciseObjects.Add(new GameObjectList.ClickedObjectData(hiddenObjects[i].itemType, false));
        }

        audioInstructions.setItemsToFind(itemsAudio);
    }
}