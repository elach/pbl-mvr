﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class CorridorManager : MonoBehaviour
{
    

    private static CorridorManager _instance;
    public static CorridorManager instance { get { return _instance; } }

    public bool returnMode, persistantNumbers;
    private bool checkPointReached = false, started = false, tutorial = true;

    [SerializeField] private LayerMask layer;
    [SerializeField] private TMP_Text numberText;
    [SerializeField] private Transform[] positions;
    [SerializeField] private GameObject[] pointObjects;
    [SerializeField] private GameObject[] circles;
    [SerializeField] private GameObject endScreen;
    [SerializeField] private TMP_Text timeScore;

    private float timeStart, timeEnd;


    private int waypointsCount = 2, currentWaypointIndex = 0;
    public int[] waypoints;

    [SerializeField] private LineRenderer lineRenderer;

    private List<int> points;


    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Debug.LogWarning("Two Corridor Managers!");
        }
        else
        {
            _instance = this;
        }

        
        points = new List<int>();
        for (int i = 0; i < 6; i++)
        {
            points.Add(i);
        }
        


    }

    private void Update()
    {
        if (started && tutorial)
        {
            lineRenderer.SetPositions(FindPath());
            foreach (GameObject go in pointObjects)
            {
                go.SetActive(false);
            }
            pointObjects[waypoints[currentWaypointIndex] - 1].SetActive(true);
        }
        else if (started && !tutorial)
        {
            foreach (GameObject circle in circles)
            {
                circle.SetActive(false);
            }
        }
        
    }

    private Vector3[] FindPath()
    {
        List<Vector3> path = new List<Vector3>();
        path.Add(WheelChairController.instance.transform.position.YAs(3));
        path.Add(positions[waypoints[currentWaypointIndex] - 1].position.YAs(3));
        return path.ToArray();
    }



    public void FillWaypoints()
    {
        waypoints = new int[waypointsCount];
        for (int i = 0; i < waypointsCount; i++)
        {
            List<int> js = new List<int>();
            for(int j = 0; j < points.Count; j++)
            {
                if (!Physics.Linecast( (i==0)? (WheelChairController.instance.transform.position.YAs(4)) : (positions[waypoints[i-1]-1].position.YAs(4)),
                    positions[points[j]].position.YAs(4), layer.value, QueryTriggerInteraction.Ignore))
                {
                    js.Add(j);
                }
            }

            int random = js.RandomElement<int>();

            waypoints[i] = points[random]+1;
            points.RemoveAt(random);
        }
        started = true;
    }

    private void End()
    {
        started = false;

        timeEnd = Time.time;
        timeScore.text = timeEnd.ToString();
        endScreen.SetActive(true);
        Debug.Log("Win: " + (timeEnd-timeStart));
    }

    private void TurnBack()
    {
        started = false;
        if (!tutorial)
        {
            foreach (GameObject go in pointObjects)
            {
                go.SetActive(true);
            }
            lineRenderer.positionCount = 0;
            timeStart = Time.time;
            started = true;
        }
    }


    public void WaypointEntered(int number)
    {
        if ((waypoints.Length > currentWaypointIndex && 0 <= currentWaypointIndex) && waypoints[currentWaypointIndex] == number)
        {
            currentWaypointIndex += checkPointReached? -1: 1;
            if (currentWaypointIndex == waypointsCount)
            {
                checkPointReached = true;
                currentWaypointIndex--;
                currentWaypointIndex--;
                if (!returnMode)
                {
                    tutorial = false;
                }
                TurnBack();
            }
            if (currentWaypointIndex < 0 && checkPointReached)
            {
                if (returnMode)
                {
                    currentWaypointIndex = 0;
                    tutorial = false;
                    checkPointReached = false;
                    returnMode = false;
                }
                else
                {
                    End();
                }
                
            }
        }
    }

    public void WaypointCountIncrement()
    {
        if (waypointsCount < 5)
        {
            waypointsCount++;
            numberText.text = waypointsCount.ToString();
        }
    }

    public void WaypointCountDecrement()
    {
        if(waypointsCount > 2)
        {
            waypointsCount--;
            numberText.text = waypointsCount.ToString();
        }
    }

    public void ReturnModeOn() 
    {
        returnMode = true;
    }

    public void ReturnModeOff()
    {
        returnMode = false;
    }

    public void PersistantNumbers()
    {
        persistantNumbers = !persistantNumbers;
    }

    public void LoadScene(int index)
    {
        SceneManager.LoadScene(index);
    }
}
