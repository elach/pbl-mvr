﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CorridorPoint : MonoBehaviour
{
    [SerializeField] Transform numberTransform;
    [SerializeField] int number;

    private void Update()
    {
        numberTransform.LookAt(WheelChairController.instance.transform.position);
    }

    private void OnTriggerEnter(Collider other)
    {
        CorridorManager.instance.WaypointEntered(number);
    }

}
