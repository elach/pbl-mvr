﻿Shader "Hidden/Glaucoma"
{
    Properties
    {
        [HideInInspector] _MainTex("", 2D) = "white" {}
        _Radius("Vignette Radius", Range(0,2.5)) = 1
        _Fade("Vignette Fade", Range(0,5)) = 2
    }

    CGINCLUDE
    #include "UnityCG.cginc"

    sampler2D _MainTex;
    float _Radius;
    float _Fade;

    struct appdata
    {
        float4 vertex : POSITION;
        float2 uv : TEXCOORD0;
    };

    struct v2f
    {
        float4 pos : SV_POSITION;
        float2 uv : TEXCOORD0;
    };

    v2f vert(appdata v)
    {
        v2f i;
        i.pos = UnityObjectToClipPos(v.vertex);
        i.uv = v.uv;
        return i;
    }

    ENDCG

    SubShader{
        Cull Off
        ZTest Always
        ZWrite Off

        Pass {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            fixed4 frag(v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.uv);
                //wyliczanie dystansu między środkiem obrazu a przetwarzanym pikselem
                float distanceFromCenter = distance(i.uv.xy,float2 (0.5, 0.5));
                //interpolacja wartości 
                float result = smoothstep(_Radius, _Radius - _Fade, distanceFromCenter);
                col *= result;

                return col;
            }

            ENDCG
        }
    }
}