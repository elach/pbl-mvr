﻿Shader "Hidden/OldSightedness"
{
	Properties {
		[HideInInspector] _MainTex ("Texture", 2D) = "white" {}
		_DepthLevel("Depth Level", float) = 1
		_DepthOffset("Depth Offset", float) = 0
	}

	CGINCLUDE
		#include "UnityCG.cginc"

//		float _BokehRadius, _FocusDistance, _FocusRange, _Samples;

		sampler2D _MainTex;
		sampler2D _CameraDepthTexture;
		sampler2D sampler_MainTex;
		half _DepthLevel;
		half _DepthOffset;
		half _Focus;
		half4 _MainTex_TexelSize;

		//struct VertexData {
		//	float4 vertex : POSITION;
		//	float2 uv : TEXCOORD0;
		//};

		//struct v2f {
		//	float4 pos : SV_POSITION;
		//	float2 depth : TEXCOORD0;
		//};

		//v2f vert(appdata_base v) {
		//	v2f i;
		//	i.pos = UnityObjectToClipPos(v.vertex);
		//	UNITY_TRANSFER_DEPTH(i.depth);
		//	return i;
		//}

	ENDCG

	//SubShader {
	//	Tags{ "RenderType" = "Opaque" }

	//	Pass { // 0 circleOfConfusionPass
	//		CGPROGRAM
	//		#pragma vertex vert
	//		#pragma fragment frag
	//		#pragma target 2.0


	//			half4 frag(v2f i) : SV_Target {
	//				
	//				float depth = SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture , i.depth);
	//				depth = LinearEyeDepth(depth);
	//				//
	//				//float coc = (depth - _FocusDistance) /
	//				//_FocusRange;
	//				
	//				// normalizacja wartosci CoC < -1 , 1 >
	//				//coc = clamp(coc , -1 , 1) * _BokehRadius;
	//				return depth;

	//			}
	//		ENDCG
	//	}		
	//}

	SubShader{
	Tags { "RenderType" = "Opaque" }
	Pass {
		CGPROGRAM

		#pragma vertex vert
		#pragma fragment frag
		#include "UnityCG.cginc"

		struct v2f {
			float4 pos : SV_POSITION;
			float2 depth : TEXCOORD0;
		};

		v2f vert(appdata_base v) {
			v2f o;
			o.pos = UnityObjectToClipPos(v.vertex);
			UNITY_TRANSFER_DEPTH(o.depth);
			return o;
		}

		half4 frag(v2f i) : SV_Target {
			UNITY_OUTPUT_DEPTH(i.depth);
		}
		ENDCG
	}
}
}


