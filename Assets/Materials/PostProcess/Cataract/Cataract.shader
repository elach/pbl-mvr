﻿Shader "Hidden/Cataract"
{
	Properties
	{
			[HideInInspector] _MainTex("", 2D) = "white" {}
			[Header(Blur)]
			[Space(10)]
			_BlurSize("Blur Size", Range(0,0.1)) = 0
			_BlurSamples("Blur Samples", Range(0,60)) = 0
			[PowerSlider(3)]_StandardDeviation("Standard Deviation", Range(0.00, 0.3)) = 0.0
			
			[Header(Whitening)]
			[Space(10)]
			_HueShift("Hue Shift", Range(0, 1)) = 0
			_SaturationPower("Saturation Adjustment", Range(0,1)) = 1
			_ValuePower("Brightness Adjustment", Range(0,1)) = 1
			
			[Header(Lambency)]
			[Space(10)]
			_LambencyIntensity("Lambency Intensity", Range(1, 10)) = 1
	}

	CGINCLUDE
	#include "UnityCG.cginc"

	sampler2D _MainTex;
	float _BlurSize;
	float _StandardDeviation;
	float _BlurSamples;

	float _HueShift;
	float _SaturationPower;
	float _ValuePower;

	float _LambencyIntensity;

	#define PI 3.14159265359
	#define E 2.71828182846

	static const float stDevSquared = _StandardDeviation * _StandardDeviation;
	static const float offsetValueDenominator = (_BlurSamples - 1) - 0.5;
	static const float gaussPart = (1 / sqrt(2 * PI * _StandardDeviation * _StandardDeviation));
	static const float TwoTimesstDevSquared = 2 * _StandardDeviation * _StandardDeviation;

	struct appdata
	{
		float4 vertex : POSITION;
		float2 uv : TEXCOORD0;
	};

	struct v2f
	{
		float4 pos : SV_POSITION;
		float2 uv : TEXCOORD0;
	};

	v2f vert(appdata v)
	{
		v2f i;
		i.pos = UnityObjectToClipPos(v.vertex);
		i.uv = v.uv;
		return i;
	}

	float3 Hue(float H)
	{
		float R = abs(H * 6 - 3) - 1;
		float G = 2 - abs(H * 6 - 2);
		float B = 2 - abs(H * 6 - 4);
		return saturate(float3(R, G, B));
	}

	float3 HSVtoRGB(in float3 HSV)
	{
		return ((Hue(HSV.x) - 1) * HSV.y + 1) * HSV.z;
	}
	
	float3 RGBtoHSV(in float3 RGB)
	{
		float3 HSV = 0;
		HSV.z = max(RGB.r, max(RGB.g, RGB.b));
		float M = min(RGB.r, min(RGB.g, RGB.b));
		float C = HSV.z - M;
		if (C != 0)
		{
			HSV.y = C / HSV.z;
			float3 Delta = (HSV.z - RGB) / C;
			Delta.rgb -= Delta.brg;
			Delta.rg += float2(2, 4);
			if (RGB.r >= HSV.z)
			{
				HSV.x = Delta.b;
			}
			else if (RGB.g >= HSV.z)
			{
				HSV.x = Delta.r;
			}
			else
			{
				HSV.x = Delta.g;
			}
			HSV.x = frac(HSV.x / 6);
		}
		return HSV;
	}


	ENDCG

	SubShader{
		Cull Off
		ZTest Always
		ZWrite Off

		//Vertical Blur pass
		Pass {
			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag

			//the fragment shader
			fixed4 frag(v2f i) : SV_TARGET{
			//failsafe so we can use turn off the blur by setting the deviation to 0
				if (_StandardDeviation == 0)
				{
					return tex2D(_MainTex, i.uv);
				}

				//init color variable
				float4 col = 0;

				float sum = 0;

				//iterate over blur samples
				for (float index = 0; index < _BlurSamples; index++) {
					//get the offset of the sample
					float offset = (index / offsetValueDenominator) * _BlurSize;
					//get uv coordinate of sample
					float2 uv = i.uv + float2(0, offset);
					//calculate the result of the gaussian function
					float gauss = gaussPart * pow(E, -((offset * offset) / TwoTimesstDevSquared));
					//add result to sum
					sum += gauss;
					//multiply color with influence from gaussian function and add it to sum color
					col += tex2D(_MainTex, uv) * gauss;
				}
				//divide the sum of values by the amount of samples
				col = col / sum;
				return col;
			}

			ENDCG
		}
		
		//Horizontal Blur pass
		Pass {
			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag


			//the fragment shader
			fixed4 frag(v2f i) : SV_TARGET{

				if (_StandardDeviation == 0)
				{
					return tex2D(_MainTex, i.uv);
				}

					//calculate aspect ratio
					float invAspect = _ScreenParams.y / _ScreenParams.x;
					//init color variable
					float4 col = 0;
					float sum = 0;

					//iterate over blur samples
					for (float index = 0; index < _BlurSamples; index++) {
						//get the offset of the sample
						float offset = (index / offsetValueDenominator) * _BlurSize * invAspect;
						//get uv coordinate of sample
						float2 uv = i.uv + float2(offset, 0);
						//calculate the result of the gaussian function
						float gauss = gaussPart * pow(E, -((offset * offset) / TwoTimesstDevSquared));
						//add result to sum
						sum += gauss;
						//multiply color with influence from gaussian function and add it to sum color
						col += tex2D(_MainTex, uv) * gauss;
					}
					//divide the sum of values by the amount of samples
					col = col / sum;
					return col;
			}

			ENDCG
		}
		
		//Whitening
		Pass {
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			//fragment shader
			fixed4 frag(v2f i) : SV_TARGET
			{
				//zapisanie obecnej wartości koloru piksela
				float3 col = tex2D(_MainTex, i.uv);
				//stworzenie koloru hsv na potrzeby następującego przypisania 
				float3 hsv = float3 (_HueShift, _SaturationPower, _ValuePower);
				//konwersja obecnego piksela
				float3 rgb2hsvCol = RGBtoHSV(col);

				//przypisanie wartości Hue , Saturation , Value do przekonwertowanego piksela
				rgb2hsvCol.x *= hsv.x;
				rgb2hsvCol.y *= hsv.y;
				rgb2hsvCol.z *= hsv.z;

				//powrotna konwersja
				float3 result = HSVtoRGB(rgb2hsvCol);
				return float4(result, 1);
			}
			ENDCG
		}
		
		//Lambency
		Pass 
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			fixed4 frag(v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);

				//col *= _LambencyIntensity;
				fixed4 tempCol = col * _LambencyIntensity;
				fixed4 result = sqrt(tempCol);

				return result;
			}
			ENDCG
		}
	}
}
