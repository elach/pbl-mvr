﻿Shader "Hidden/YellowEye"
{
    Properties
    {
        [HideInInspector] _MainTex("", 2D) = "white" {}
        [HideInInspector]_YellowEyeColor("YellowEyeColor", Color) = (1,1,0.702,1)
    }

    CGINCLUDE
    #include "UnityCG.cginc"

    sampler2D _MainTex;
    float4 _YellowEyeColor;

    struct appdata
    {
        float4 vertex : POSITION;
        float2 uv : TEXCOORD0;
    };

    struct v2f
    {
        float4 pos : SV_POSITION;
        float2 uv : TEXCOORD0;
    };

    v2f vert(appdata v)
    {
        v2f i;
        i.pos = UnityObjectToClipPos(v.vertex);
        i.uv = v.uv;
        return i;
    }

    ENDCG

    SubShader{
        Cull Off
        ZTest Always
        ZWrite Off

        Pass {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            fixed4 frag(v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.uv);
                return col * _YellowEyeColor;
            }
            ENDCG
        }
    }
}